(function () {
  'use strict';

   angular
  .module('admin')
  .config(routeConfig);

  function routeConfig($stateProvider) {
    $stateProvider
    .state('admin-home', {
      url: '/admin/home',
      templateUrl: '/modules/admin/client/views/admin-home.client.view.html',
      moduleId: '58d8149528ec002b895aff0c',
      controller: ''
    });


  }

}());
