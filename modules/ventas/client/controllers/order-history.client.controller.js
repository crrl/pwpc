(function () {
  'use strict'

  function orderHistoryCtrl ($scope, FoliosAPI, ticketAPI, localStorage) {

    var currentTime = new Date();
    var initialDate;
    var finalDate;

    $scope.currentTime = currentTime;
    $scope.month = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
                    'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    $scope.monthShort = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
    $scope.weekdaysFull = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
    $scope.weekdaysLetter = ['D', 'L', 'M', 'X', 'J', 'V', 'S'];
    $scope.disable = [];
    $scope.today = 'Hoy';
    $scope.clear = 'Limpiar';
    $scope.close = 'Cerrar';
    var days = 15;
    $scope.minDate = moment('2017-01-01').toISOString();
    $scope.maxDate = (new Date($scope.currentTime.getTime() + ( 1000 * 60 * 60 *24 * days ))).toISOString();

    function initialInfo() {
      initialDate = '';
      finalDate = '';
      
      FoliosAPI.getHistory({skip:0}).$promise
      .then(function (res) {
        $scope.history = res.result;
        $scope.count = res.count;
        $scope.history = $scope.history.reverse();
        $scope.history.forEach(function (item) {
          if (!item.products) {
            item.products = [];
          }

          item.details.forEach(function (detail) {
            detail.products.forEach(function (product) {
              var index = _.findIndex(item.products, {productId: product.productId});
              if (index >= 0) {
                item.products[index].quantity =
                  parseFloat(item.products[index].quantity) +
                  parseFloat(product.quantity);
              } else {
                item.products.push({
                  productId: product.productId,
                  productName: product.productName,
                  price: product.price,
                  quantity: parseFloat(product.quantity)
                });
              }
            });
          });
        });
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error, favor de intentar más tarde', 3000);
      });
    }

    $scope.setPage = function(pageNumber, previousPage) {
      if(!pageNumber && !previousPage) {
        return;
      }
      initialDate = '';
      finalDate = '';
      FoliosAPI.getHistory({skip:((pageNumber.newPageNumber*6) || pageNumber*6) -6, status:$scope.status}).$promise
      .then(function (res) {
        $scope.history = res.result;
        $scope.count = res.count;
        $scope.history = $scope.history.reverse();
        $scope.history.forEach(function (item) {
          if (!item.products) {
            item.products = [];
          }

          item.details.forEach(function (detail) {
            detail.products.forEach(function (product) {
              var index = _.findIndex(item.products, {productId: product.productId});
              if (index >= 0) {
                item.products[index].quantity =
                  parseFloat(item.products[index].quantity) +
                  parseFloat(product.quantity);
              } else {
                item.products.push({
                  productId: product.productId,
                  productName: product.productName,
                  price: product.price,
                  quantity: parseFloat(product.quantity)
                });
              }
            });
          });
        });
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error, favor de intentar más tarde', 3000);
      });
    };

    $scope.filter = function () {
      if (!$scope.initialDate || !$scope.finalDate) {
        Materialize.toast('Favor de seleccionar fechas válidas', 3000);
        return;
      }
      $scope.selectCanceled = false;
      $scope.selectAll = true;
      $scope.selectActive = false;
      $scope.showFilteredData = true;
      initialDate = angular.copy($scope.initialDate);
      finalDate = angular.copy($scope.finalDate);

      initialDate = initialDate.split('/');
      finalDate = finalDate.split('/');

      initialDate = initialDate[2] + '-' + initialDate[1] + '-' + initialDate[0];
      finalDate = finalDate[2] + '-' + finalDate[1] + '-' + finalDate[0];

      if (moment(initialDate).diff(moment(finalDate), 'days') > 0) {
        Materialize.toast('Favor de seleccionar fechas válidas', 3000);
        return;
      }
      FoliosAPI.getFilteredHistory({
        initialDate: initialDate,
        finalDate: finalDate
      }).$promise
      .then(function (res) {
          
        $scope.history = res.result;
        $scope.history = $scope.history.reverse();
        $scope.history.forEach(function (item) {
          if (!item.products) {
            item.products = []
          }

          item.details.forEach(function (detail) {
            detail.products.forEach(function (product) {
              var index = _.findIndex(item.products, {productId: product.productId});
              if (index >= 0) {
                item.products[index].quantity =
                  parseFloat(item.products[index].quantity) +
                  parseFloat(product.quantity);
              } else {
                item.products.push({
                  productId: product.productId,
                  productName: product.productName,
                  price: product.price,
                  quantity: parseFloat(product.quantity)
                });
              }
            });
          });
        });
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error, favor de intentar más tarde', 3000);
      });
    };

    $scope.clean = function () {
      if (!$scope.showFilteredData) {
        return;
      }
      $scope.initialDate = ''; 
      $scope.finalDate = '';
      $scope.showFilteredData = false;
      $scope.status = '';
      $scope.selectCanceled = false;
      $scope.selectAll = true;
      $scope.selectActive = false;
      $scope.setPage(1);
    };

    $scope.showOrders = function (param, selected, setPage) { 
      if (!setPage) {
        $scope.status = param;
        $scope.setPage(1);
      }
      $scope.selectCanceled = false;
      $scope.selectAll = false;
      $scope.selectActive = false;
      $scope.selectIntern = false;
      $scope[selected] = true;
      $scope.orderFilter = param;
    };

    $scope.getTickets = function (folio) {
      ticketAPI.query({folio:folio}).$promise
      .then( function (res) {
        $scope.tickets = res;
        angular.element('#ticket-list-modal').modal();
        angular.element('#ticket-list-modal').modal('open');
      });
    };

    $scope.openResumeModal = function () {
      $scope.showingDate = '';
      $scope.products = [];
      $scope.totalSold = 0;
      $scope.totalLost = 0;

      $scope.history.forEach(function (item) {
        var currentStatus = item.status;
        if (item.table === 9998) {
          return;
        }
        item.products.forEach(function (product) {
          var index = _.findIndex($scope.products, {
            productId: product.productId,
            status: currentStatus});
            if (currentStatus === 'Cancelada') {
              $scope.totalLost += (parseFloat(product.price) * parseFloat(product.quantity)) || 0;
            } else {
              $scope.totalSold += (parseFloat(product.price) * parseFloat(product.quantity)) || 0;
            }
          if (index >= 0) {
            $scope.products[index].quantity =
              parseFloat($scope.products[index].quantity) +
              parseFloat(product.quantity);
          } else {
            $scope.products.push({
              status: currentStatus,
              productId: product.productId,
              productName: product.productName,
              price: product.price,
              quantity: parseFloat(product.quantity)
            });
          }
        });
      });
      if (initialDate && finalDate) {
        if (initialDate.includes('-')) {
          initialDate = initialDate.split('-');
          finalDate = finalDate.split('-');
          initialDate = initialDate[2] + '/' + initialDate[1] + '/' + initialDate[0];
          finalDate = finalDate[2] + '/' + finalDate[1] + '/' + finalDate[0];
        }
        $scope.showingDate = initialDate + ' A ' + finalDate;
      }

      angular.element('#orders-resume-modal').modal();
      angular.element('#orders-resume-modal').modal('open');
    };

    initialInfo();
  }

  angular.module('ventas').controller('orderHistoryCtrl', [
    '$scope',
    'FoliosAPI',
    'ticketAPI',
    'localStorageService',
    orderHistoryCtrl
  ]);
}());
