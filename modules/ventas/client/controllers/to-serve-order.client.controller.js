(function () {
    'use strict';
    function toServeOrderCtrl($scope, FoliosAPI, localStorage, SOCKET) {
      var currentItem;
      var audio = new Audio('/modules/core/client/sounds/notification.wav');
      SOCKET.on('refreshPendingServe', function(data) {
        $scope.$apply(function () {
          $scope.pendingServe = data.msg;
          $scope.pendingServe.forEach(function (item, index) {
            $scope.pendingServe[index].pendingProducts = 0;
            item.details.forEach(function (detail, detailIndex) {
              $scope.pendingServe[index].details[detailIndex].pendingFromOrder = 0;
              detail.products.forEach(function (product) {
                if (!product.served) {
                  $scope.pendingServe[index].details[detailIndex].pendingFromOrder += 1;
                  $scope.pendingServe[index].pendingProducts += 1;
                }
              });
            });
          });
          audio.play();
        });
      });
  
      function initialInfo() {
        FoliosAPI.getPendingService().$promise
        .then(function (res) {
          $scope.pendingServe = res;
          $scope.pendingServe.forEach(function (item, index) {
            $scope.pendingServe[index].pendingProducts = 0;
            item.details.forEach(function (detail, detailIndex) {
              $scope.pendingServe[index].details[detailIndex].pendingFromOrder = 0;
              detail.products.forEach(function (product) {
                if (!product.served) {
                  $scope.pendingServe[index].details[detailIndex].pendingFromOrder += 1;
                  $scope.pendingServe[index].pendingProducts += 1;
                }
              });
            });
          });
        })
        .catch(function (err) {
          Materialize.toast('Ha ocurrido un error, favor de intentar más tarde', 3000);
        });
      }
  
      $scope.finishServingModal = function (item) {
        currentItem = item;
        angular.element('#finish-serving-modal').modal();
        angular.element('#finish-serving-modal').modal('open');
      };
  
      $scope.finishServing = function () {
        var showError = false;
        var tempIndex = _.findIndex($scope.pendingServe, { _id: currentItem._id });
        $scope.pendingServe[tempIndex].details.forEach(function (detail) {
          detail.products.forEach(function (product) {
            if (product.served || product.kitchen !== false) showError = true;
          });
        });
        if (showError) {
            Materialize.toast('Pedido no terminado, intente más tarde', 1000);
            return;
        }
        FoliosAPI.updatePendingService({ id: currentItem.folioId }, {
          served: 'si',
          details: currentItem.details
        }).$promise
        .then(function () {
          initialInfo();
        })
        .catch(function () {
          Materialize.toast('Ha ocurrido un error, intente más tarde', 1000);
        });
      };
  
      $scope.updatePartialDetail = function (item) {
        FoliosAPI.getPartialPendingService({ id: item.folioId }, {
          details: item.details
        }).$promise
        .then(function () {
        })
        .catch(function () {
          Materialize.toast('Ha ocurrido un error, intente más tarde', 3000);
        });
      };
  
      initialInfo();
  
    }
  
    angular.module('ventas').controller('toServeOrderCtrl', [
      '$scope',
      'FoliosAPI',
      'localStorageService',
      'SOCKET',
      toServeOrderCtrl
    ]);
  }());
  