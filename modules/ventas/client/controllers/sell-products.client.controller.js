(function () {
  'use strict';

  function sellProductsCtrl($scope, $rootScope, $state, FoliosAPI, ProductsAPI, GlobalUserAPI,
                            FondoAPI, ConfigAPI, ProductCategoryAPI, localStorage, SOCKET, ticketAPI, PrintersAPI) {
    var printedOrder = true;
    var orderToAdd = [];
    var mywindow;
    var toFinish = false;
    var mesa = '';
    var mesaNum = 0;
    var initialLoaded = false;
    var kitchenPrint = '';
    var printedArray = [];
    var totalOrder = [];
    var orderIndex;
    var folioIndex;
    var isAbleToOrder = false;
    var kitchenPrintTime;
    var clientNumber;
    var presentPrinters = [];
    var reset = true;
    var currentIp = '';
    var alreadyPrintingFolios = [];
    var comment = '';
    SOCKET.on('ping', function(data) {
      SOCKET.emit('pong', {beat: 1});
    });

    SOCKET.on('refreshPendingPayment', function(data) {
      $scope.$apply(function () {
        mywindow = {};
        $scope.pendingFolios = data.msg;
        if ($rootScope.printOrders) {
          $scope.pendingFolios.forEach(function (folio, index, pendingFolioArray) {
            comment = '';
            if (alreadyPrintingFolios.indexOf(folio._id) >= 0) {
              return;
            } else {
              alreadyPrintingFolios.push(folio._id);
            }
            reset = true;
            toFinish = false;
            printedOrder = true;
            orderToAdd = [];
            kitchenPrint = '';
            
            presentPrinters = [];

            folio.dateHour = moment(folio.date).startOf().fromNow();
            kitchenPrint = '';
            folio.details.forEach(function (detail, detailIndex, detailArray) {
              if (detail.comment) {
                comment += 'platillo #' + detail.clientNumber + ':\n' + detail.comment + '\n';
              }

              detail.orderToPrint = [];
              if (!printedOrder) {
                orderToAdd.push({
                  mesa: folio.table,
                  Platillo: detail.clientNumber,
                  // comment: detail.comment,
                  togo: detail.toGo,
                  orders: []
                });
              }
              $scope.printers.forEach(function (printer, printersIndex, printersArray) {
                kitchenPrint = '';
                kitchenPrint = '-----------------------\n';
                detail.products.forEach(function (product, productIndex, productArray) {
                  
                  if (product.printed) {
                    toFinish = true;
                    
                    return;
                  } else if (product.printer !== printer.name) {
                    
                    return;
                  } else if (!presentPrinters.includes(product.printer)) presentPrinters.push(product.printer);
                  if (kitchenPrint.indexOf('platillo #') === -1) {
                    kitchenPrint += 'platillo #' + detail.clientNumber + ':\n';
                  }
                  orderIndex = _.findIndex(totalOrder, {printer: printer.name});
                  if (orderIndex >= 0 &&
                     totalOrder[orderIndex].printer === printer.name) {
                    if (!printedOrder && product.printed === false) {
                      orderToAdd[orderToAdd.length - 1].orders.push(product);                      
                      if (!orderToAdd[orderToAdd.length - 1].togo) {
                        kitchenPrint += product.quantity + '  -  ' + product.productName + '\n';
                      } else {
                        kitchenPrint += product.quantity + '  -  ' + product.productName + ' - P/LL' + '\n';
                      }
                      $scope.pendingFolios[index].details[detailIndex].orderToPrint.push(product);
                      /* if (productIndex === productArray.length - 1 && orderToAdd[orderToAdd.length - 1].comment !== '') {
                        kitchenPrint += '\n';
                        kitchenPrint +=  'Comentario: \n';
                        kitchenPrint +=  orderToAdd[orderToAdd.length - 1].comment + '\n';
                      }*/
                    }
                    totalOrder[orderIndex].kitchenPrint += kitchenPrint;
                    kitchenPrint = '';
                    
                    return;
                  }

                  if (!product.printed && printedOrder) {
                    printedOrder = false;
                    orderToAdd.push({
                      mesa: folio.table,
                      Platillo: detail.clientNumber,
                      // comment: detail.comment,
                      togo: detail.toGo,
                      printer: product.printer,
                      orders: []
                    });
                    mesaNum = folio.table;
                    if (folio.table === 9999) {
                      mesa = '';
                    } else if (folio.table === 9998) {
                      mesa = 'Consumo interno';
                    } else {
                      mesa = 'Mesa #' + folio.table;
                    }
                  }
                  if (!printedOrder && !product.printed) {
                    orderToAdd[orderToAdd.length - 1].orders.push(product);
                    if (!orderToAdd[orderToAdd.length - 1].togo) {
                      kitchenPrint += product.quantity + '  -  ' + product.productName + '\n';
                    } else {
                      kitchenPrint += product.quantity + '  -  ' + product.productName + ' - P/LL' + '\n';
                    }
                    $scope.pendingFolios[index].details[detailIndex].orderToPrint.push(product);
                    /* if (productIndex === productArray.length - 1 && orderToAdd[orderToAdd.length - 1].comment !== '') {
                      kitchenPrint += '\n';
                      kitchenPrint += 'Comentario: \n';
                      kitchenPrint += orderToAdd[orderToAdd.length - 1].comment + '\n';
                    }*/
                  }
                  if (!product.printed) {
                    reset = false;
                  }
                  if (printedArray.includes($scope.pendingFolios[index].folio && reset)) {
                    return;
                  }
                });
                isAbleToOrder = _.find(totalOrder, { printer: printer.name });
                kitchenPrintTime = 'Mesa #' + folio.table + '\n';
                kitchenPrintTime += '-----------------------\n';
                kitchenPrintTime += '   ' + moment().format('hh:mm:a') + '\n';
                if (!printedOrder && orderIndex < 0 && !isAbleToOrder && !_.isEmpty(kitchenPrint)) {
                  totalOrder.push({
                    table: mesaNum,
                    printer: printer.name,
                    kitchenPrint: kitchenPrintTime + kitchenPrint,
                    printed: false,
                    folio: folio._id
                  });
                  kitchenPrint = '';
                }
              });
            });
            if (!$scope.pendingFolios[index].printed && !printedArray.includes($scope.pendingFolios[index].folio)) {
              $scope.pendingFolios[index].printed = true;
              printedArray.push($scope.pendingFolios[index].folio);
            }
            totalOrder.forEach(function (order, index) {
              if (order.folio === folio._id) {
                order.comment = comment;
              }
            });
          });
          totalOrder.forEach(function (order) {
            while (order.kitchenPrint.indexOf('-----------------------\n-----------------------\n') > -1) {
              order.kitchenPrint = order.kitchenPrint.replace(new RegExp('-----------------------\n-----------------------\n', 'g'), '-----------------------\n');
            }
            if (order.kitchenPrint.length <= 70) {
              return;
            }
            /* if (presentPrinters.indexOf(order.printer) < 0) {
              return;
            }*/
            if (!order.printed) {
              if (order.table === 9999) {
                order.kitchenPrint += '-----------------------\n';
                order.kitchenPrint += 'TODO PARA LLEVAR\n';
                order.kitchenPrint += '-----------------------\n';
              }
              if (toFinish) {
                order.kitchenPrint += '-----------------------\n';
                order.kitchenPrint += 'PARA TERMINAR    \n';
              }
              if (order.comment) {
                order.kitchenPrint += '-----------------------\n';
                order.kitchenPrint += 'Comentario:\n' + order.comment;
              }

              order.printed = true;

              while (order.kitchenPrint.indexOf('-----------------------\n-----------------------\n') > -1) {
                order.kitchenPrint = order.kitchenPrint.replace(new RegExp('-----------------------\n-----------------------\n', 'g'), '-----------------------\n');
              }
              ProductsAPI.printKitcken({
                _printerName: order.printer || 'Cocina',
                dataToBeWritten: order.kitchenPrint,
                table: mesaNum
              }).$promise
              .then(function (res) {
                FoliosAPI.setTicketToTrue({
                  id: order.folio
                }).$promise
                .then(function (res) {
                  alreadyPrintingFolios = [];
                });
              })
              .catch(function (err) {
                alreadyPrintingFolios = [];
              });
            }
          });
          totalOrder = [];
        }
      });
    });

    $rootScope.business = localStorage.get('business');
    $scope.businessName = $rootScope.business.name;
    $scope.businessAdress = $rootScope.business.address;
    $scope.currentTime = moment().format('DD-MM-YYYY');
    $scope.currentHour = moment().format('hh:mm:a');
    var selectedElement;
    var currentIndex;
    var toDeleteClientIndex;
    var products = [];
    var subTotal = 0;
    var ticket;
    var tempUsers = [];
    $scope.isNew = true;
    var sellId;
    var temppendingFolios;
    var tempIndex;
    var selectedFolioToDelete;
    var temporalProducts = [];
    var orderSubTotal = 0;
    var preventSave = false;
    var preventMultipleSave = false;
    var total = 0;
    var pending = 0;
    var employmentId = localStorage.get('empresa');
    var userId = localStorage.get('_id');
    var user = localStorage.get('user');
    var fondo = localStorage.get('fondo');
    $scope.orderToShow = [];
    $scope.clients = [];
    $scope.selectedItem = {};
    $scope.selectedProductCategory = 0;
    $scope.quantityString = '';


    function initialInfo() {
      PrintersAPI.query().$promise
      .then( function (res) {
        $scope.printers = res;
      });
      FondoAPI.query().$promise
      .then(function (res) {
        $scope.fondoList = res;
      });
      ProductCategoryAPI.query().$promise
      .then(function (res) {
        $scope.productsByCategory = res;
      })
      GlobalUserAPI.get().$promise
      .then(function (res) {
        $scope.users = res.users;
        $scope.selectedRol = _.find(res.users, { _id: userId }).rol;
        if (!fondo && $scope.selectedRol !== 'n') {
          if (localStorage.get('fondo') > 0) return;
          angular.element('#fondo-client-modal').modal();
          angular.element('#fondo-client-modal').modal('open');
        }
      });

      $scope.clients = [];

      angular.element('#new-sell-modal').modal({
        dismissible: false,
        complete: function () {
          $rootScope.loadingPage = false;
        }
      });

      angular.element('#edit-selected-client').modal({
        dismissible: false
      });

      angular.element('#verify-client-delete').modal();

      FoliosAPI.query().$promise
      .then(function (res) {
        $scope.pendingFolios = res;

        mywindow = {};
        if ($rootScope.printOrders && !initialLoaded) {
          $scope.pendingFolios.forEach(function (folio, index, pendingFolioArray) {
            comment = '';
            reset = true;
            toFinish = false;
            printedOrder = true;
            orderToAdd = [];
            kitchenPrint = '';
            presentPrinters = [];

            folio.dateHour = moment(folio.date).startOf().fromNow();
            kitchenPrint = '';
            folio.details.forEach(function (detail, detailIndex, detailArray) {
              if (detail.comment) {
                comment += 'platillo #' + detail.clientNumber + ':\n' + detail.comment + '\n';
              }

              detail.orderToPrint = [];
              if (!printedOrder) {
                orderToAdd.push({
                  mesa: folio.table,
                  Platillo: detail.clientNumber,
                  // comment: detail.comment,
                  togo: detail.toGo,
                  orders: []
                });
              }

              $scope.printers.forEach(function (printer, printersIndex, printersArray) {
                kitchenPrint = '';
                kitchenPrint = '-----------------------\n';
                detail.products.forEach(function (product, productIndex, productArray) {
                  
                  
                  if (product.printed) {
                    toFinish = true;
                    return;
                  } else if (product.printer !== printer.name) {
                    return;
                  } else if (!presentPrinters.includes(product.printer)) presentPrinters.push(product.printer);
                  orderIndex = _.findIndex(totalOrder, {printer: printer.name, table: folio.table });
                  if (orderIndex >= 0 &&
                     totalOrder[orderIndex].printer === printer.name) {
                    if (!printedOrder && product.printed === false) {
                      orderToAdd[orderToAdd.length - 1].orders.push(product);
                      if (!orderToAdd[orderToAdd.length - 1].togo) {
                        kitchenPrint += product.quantity + '  -  ' + product.productName + '\n';
                      } else {
                        kitchenPrint += product.quantity + '  -  ' + product.productName + ' - P/LL' + '\n';
                      }
                      $scope.pendingFolios[index].details[detailIndex].orderToPrint.push(product);
                      /* if (productIndex === productArray.length - 1 && orderToAdd[orderToAdd.length - 1].comment !== '') {
                        kitchenPrint += '\n';
                        kitchenPrint +=  'Comentario: \n';
                        kitchenPrint +=  orderToAdd[orderToAdd.length - 1].comment + '\n';
                      }*/
                    }
                    totalOrder[orderIndex].kitchenPrint += kitchenPrint;
                    kitchenPrint = '';
                    return;
                  }
                  if (!product.printed && printedOrder) {
                    printedOrder = false;
                    orderToAdd.push({
                      mesa: folio.table,
                      Platillo: detail.clientNumber,
                      // comment: detail.comment,
                      togo: detail.toGo,
                      printer: product.printer,
                      orders: []
                    });
                    mesaNum = folio.table;
                    if (folio.table === 9999) {
                      mesa = '';
                    } else if (folio.table === 9998) {
                      mesa = 'Consumo interno';
                    } else {
                      mesa = 'Mesa #' + folio.table;
                    }
                  }
                  if (!printedOrder && !product.printed) {
                    orderToAdd[orderToAdd.length - 1].orders.push(product);
                    if (!orderToAdd[orderToAdd.length - 1].togo) {
                      kitchenPrint += product.quantity + '  -  ' + product.productName + '\n';
                    } else {
                      kitchenPrint += product.quantity + '  -  ' + product.productName + ' - P/LL' + '\n';
                    }
                    $scope.pendingFolios[index].details[detailIndex].orderToPrint.push(product);
                    /* if (productIndex === productArray.length - 1 && orderToAdd[orderToAdd.length - 1].comment !== '') {
                      kitchenPrint += '\n';
                      kitchenPrint +=  'Comentario: \n';
                      kitchenPrint +=  orderToAdd[orderToAdd.length - 1].comment + '\n';
                    }*/
                  }
                  if (!product.printed) {
                    reset = false;
                  }
                 // product.printed = true;
                  if (printedArray.includes($scope.pendingFolios[index].folio && reset)) {
                    return;
                  }
                });
                
                
                isAbleToOrder = _.find(totalOrder, { printer: printer.name, table: folio.table });
                if (!printedOrder && orderIndex < 0 && !isAbleToOrder && !_.isEmpty(kitchenPrint)) {
                  kitchenPrintTime = 'Mesa #' + folio.table + '\n';
                  kitchenPrintTime += '-----------------------\n';
                  kitchenPrintTime += '   ' + moment().format('hh:mm:a') + '\n';
                  totalOrder.push({
                    table: angular.copy(folio.table),
                    printer: printer.name,
                    kitchenPrint: kitchenPrintTime + kitchenPrint,
                    printed: false,
                    folio: folio._id,
                    toFinish: toFinish
                  });
                  kitchenPrint = '';
                }
              });
            });
            if (!$scope.pendingFolios[index].printed && !printedArray.includes($scope.pendingFolios[index].folio)) {
              $scope.pendingFolios[index].printed = true;
              printedArray.push($scope.pendingFolios[index].folio);
            }
            totalOrder.forEach(function (order, index) {
              if (order.folio === folio._id) {
                order.comment = comment;
              }
            });
          });
          totalOrder.forEach(function (order, orderIndex, orderArray) {
            while (order.kitchenPrint.indexOf('-----------------------\n-----------------------\n') > -1) {
              order.kitchenPrint = order.kitchenPrint.replace(new RegExp('-----------------------\n-----------------------\n', 'g'), '-----------------------\n');
            }
            if (order.kitchenPrint.length <= 70) {
              return;
            }
            /* if (presentPrinters.indexOf(order.printer) < 0) {
              return;
            }*/
            if (!order.printed) {
              if (order.table === 9999) {
                order.kitchenPrint += '-----------------------\n';
                order.kitchenPrint += 'TODO PARA LLEVAR\n';
                order.kitchenPrint += '-----------------------\n';
              }
              if (order.toFinish) {
                order.kitchenPrint += '-----------------------\n';
                order.kitchenPrint += 'PARA TERMINAR    \n';
              }
              if (order.comment) {
                order.kitchenPrint += '-----------------------\n';
                order.kitchenPrint += 'Comentario:\n' + order.comment;
              }
              
              order.printed = true;

              while (order.kitchenPrint.indexOf('-----------------------\n-----------------------\n') > -1) {
                order.kitchenPrint = order.kitchenPrint.replace(new RegExp('-----------------------\n-----------------------\n', 'g'), '-----------------------\n');
              }
              
              ProductsAPI.printKitcken({
                _printerName: order.printer || 'Cocina',
                dataToBeWritten: order.kitchenPrint,
                table: mesaNum
              }).$promise
              .then(function (res) {
                FoliosAPI.setTicketToTrueBack({
                  id: order.folio
                }).$promise
                .then(function (res) {
                  if (orderIndex === orderArray.length - 1) {
                    FoliosAPI.setTicketToTrue({
                      id: order.folio
                    });
                  }
                });
              });
            }
          });
          totalOrder = [];
        }
        initialLoaded = true;
        $scope.pendingFolios.forEach(function (folio) {
          folio.dateHour = moment(folio.date).startOf().fromNow();
        });

        ProductsAPI.query().$promise
        .then(function (res) {
          $scope.products = res;
          $scope.productsByCategory.forEach(function (product) {
            product.products = [];
          });
          $scope.products.forEach(function (product) {
            if(_.find($scope.productsByCategory, {_id: product.categoryId}))
              _.find($scope.productsByCategory, {_id: product.categoryId}).products.push(product);
          });
        })
        .catch(function (err) {
        });
      })
      .catch(function (err) {
        Materialize.toast('Há ocurrido un error al leer fecha del sistema.', 1000);
        localStorage.clearAll();
        $state.go('login');
        $scope.pendingFolios = [];
      });
    }

    $scope.setFondo = function () {
      if (!$scope.fondo || parseFloat($scope.fondo) <= 0) {
        Materialize.toast('Favor de capturar una cantidad válida.', 1000);
        return;
      }
      fondo = angular.copy($scope.fondo);
      FondoAPI.save({
        businessId: employmentId,
        userId: userId,
        quantity: fondo
      }).$promise
      .then(function () {
        localStorage.set('fondo', $scope.fondo);
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error al capturar valor, intente más tarde', 1000);
      });
      angular.element('#fondo-client-modal').modal('close');
    };

    $scope.selectFondo = function(quantity) {
      localStorage.set('fondo', quantity);
      angular.element('#fondo-client-modal').modal('close');
    };

    $scope.modifySelectedFolio = function (item) {
      $scope.selectedFolio = item;
      $scope.isNew = false;
      sellId = item._id;
      if (item.table < 9999) {
        $scope.sellToGo = false;
      } else {
        $scope.sellToGo = true;
      }
      if (item.table === 9998) {
        $scope.employeeFood = true;
      }
      $scope.clients = angular.copy(item.details);
      tempUsers = angular.copy(item.details);
      $scope.table = parseInt(item.table);

      $scope.pendingToPay = item.pendingPayment;
      angular.element('#new-sell-modal').modal('open');
    };

    $scope.removeSelectedItem = function (index) {
      $scope.clients[currentIndex].total -= $scope.clients[currentIndex].products[index].subTotal;
      $scope.clients[currentIndex].subTotal -= $scope.clients[currentIndex].products[index].subTotal;
      $scope.clients[currentIndex].products.splice(index,1);
    };

    $scope.openNewSellModal = function () {
      $scope.isNew = true;
      $scope.table = '';
      $scope.sellToGo = false;
      $scope.isGeneralSale = false;
      $scope.employeeFood = false;
      angular.element('#new-sell-modal').modal({
        dismissible: false,
        complete: function () {
          $rootScope.loadingPage = false;
        }
      });
      angular.element('#new-sell-modal').modal('open');
    };

    $scope.editSelectedClient = function (index) {
      $scope.selectedProductCategory = 0;
      $scope.selectedClient = $scope.clients[index];
      currentIndex = index;
      $scope.comment = angular.copy($scope.clients[currentIndex].comment);
      $scope.toGo = angular.copy($scope.clients[currentIndex].toGo);
      total = angular.copy($scope.clients[currentIndex].total);
      subTotal = angular.copy($scope.clients[currentIndex].subTotal);
      products = angular.copy($scope.clients[currentIndex].products);
      if ($rootScope.selectedSellStyle === 0) {
        angular.element('#edit-selected-client').modal('open');
      } else {
        angular.element('#edit-selected-client-images').modal();
        angular.element('#edit-selected-client-images').modal('open');
        selectedElement = document.getElementById('product-container');
        selectedElement.scrollTop = 0;
      }
    };

    $scope.addClient = function (selectedUI) {
      $scope.clients.push({
        clientNumber: $scope.clients.length + 1,
        subTotal: 0.00,
        total: 0.00,
        paid: 0.00,
        comment: '',
        products: []
      });
      $scope.editSelectedClient($scope.clients.length - 1);
    };

    $scope.openDeleteClientModal = function (index) {
      toDeleteClientIndex = index;
      angular.element('#verify-client-delete').modal('open');
    };

    $scope.deleteSelectedClient = function () {
      $scope.clients.splice(toDeleteClientIndex, 1);
    };

    $scope.addToList = function () {
      if (!$scope.quantity || $scope.quantity < 0) {
        $scope.quantity = 1;
      }
      if (!$scope.selectedSupply) {
        Materialize.toast('Favor de llenar los campos necesarios.', 1000);
        return;
      }

      $scope.clients[currentIndex].subTotal += $scope.selectedSupply.originalObject.price * $scope.quantity;
      $scope.clients[currentIndex].total += $scope.selectedSupply.originalObject.price * $scope.quantity;
      $scope.clients[currentIndex].products.push({
        served: false,
        printed: false,
        productId: $scope.selectedSupply.originalObject._id,
        productName: $scope.selectedSupply.originalObject.name,
        price: $scope.selectedSupply.originalObject.price,
        quantity: $scope.quantity,
        subTotal: $scope.selectedSupply.originalObject.price * $scope.quantity
      });
      $scope.$broadcast('angucomplete-alt:clearInput');
      $scope.quantity = 0;
      angular.element('#supplies_value').focus();
    };

    $scope.checkEnterKey = function (ev) {
      if(ev.keyCode == 13){
        $scope.addToList();
      }
    };

    $scope.addItemsToClient = function () {
      if ($scope.selectedSupply) {
        if (!$scope.quantity || $scope.quantity < 0) {
          $scope.quantity = 1;
        }
        $scope.clients[currentIndex].subTotal += $scope.selectedSupply.originalObject.price * $scope.quantity;
        $scope.clients[currentIndex].total += $scope.selectedSupply.originalObject.price * $scope.quantity;
        $scope.clients[currentIndex].products.push({
          served: false,
          printed: false,
          productId: $scope.selectedSupply.originalObject._id,
          productName: $scope.selectedSupply.originalObject.name,
          price: $scope.selectedSupply.originalObject.price,
          quantity: $scope.quantity,
          subTotal: $scope.selectedSupply.originalObject.price * $scope.quantity
        });
        $scope.$broadcast('angucomplete-alt:clearInput');
        $scope.quantity = 0;
      }

      $scope.clients[currentIndex].subTotal -= $scope.paying || 0;
      if ($scope.clients[currentIndex].subTotal < 0) $scope.clients[currentIndex].subTotal = 0;
      $scope.clients[currentIndex].comment = angular.copy($scope.comment);
      $scope.clients[currentIndex].toGo = angular.copy($scope.toGo);
      $scope.clients[currentIndex].paid = parseFloat($scope.clients[currentIndex].paid) +
                                          parseFloat($scope.paying || 0);
      if ($scope.clients[currentIndex].paid > $scope.clients[currentIndex].subTotal) {
        $scope.clients[currentIndex].paid = angular.copy($scope.clients[currentIndex].total);
      }
      $scope.paying = 0;
      $scope.comment = '';
      $scope.toGo = false;
      angular.element('#edit-selected-client').modal('close');
    };

    $scope.setSelectedItem = function (item) {
      $scope.selectedItem = item;
    };

    $scope.counterAdd = function (number) {
      $scope.quantityString += number;
    };

    $scope.counterBackspace = function () {
      $scope.quantityString = $scope.quantityString.substring(0,$scope.quantityString.length - 1);
    };

    $scope.imageMenuAddToList = function () {
      $scope.printer = $scope.productsByCategory[$scope.selectedProductCategory].printer || 'Cocina';
      $scope.quantity = angular.copy(parseFloat($scope.quantityString));
      $scope.quantityString = '';
      if (!$scope.quantity || $scope.quantity < 0) {
        $scope.quantity = 1;
      }
      if (!$scope.selectedItem || !$scope.selectedItem.price) {
        Materialize.toast('Favor de Seleccionar un producto.', 1000);
        return;
      }

      $scope.clients[currentIndex].subTotal += $scope.selectedItem.price * $scope.quantity;
      $scope.clients[currentIndex].total += $scope.selectedItem.price * $scope.quantity;
      $scope.clients[currentIndex].products.push({
        served: false,
        printed: false,
        printer: $scope.printer,
        productId: $scope.selectedItem._id,
        productName: $scope.selectedItem.name,
        price: $scope.selectedItem.price,
        portions: $scope.selectedItem.portions,
        quantity: $scope.quantity,
        subTotal: $scope.selectedItem.price * $scope.quantity
      });
      
      $scope.quantity = 0;
      $scope.selectedItem = {};
    };

    $scope.imageMenuAddItemsToClient = function (int) {
      $scope.selectedProductCategory = 0;
      selectedElement = document.getElementById('product-container');
      selectedElement.scrollTop = 0;
      $scope.quantity = angular.copy(parseFloat($scope.quantityString));
      $scope.quantityString = '';
      // if ($scope.selectedItem && $scope.selectedItem.price) {
      //   if (!$scope.quantity || $scope.quantity < 0) {
      //     $scope.quantity = 1;
      //   }
      //   $scope.clients[currentIndex].subTotal += $scope.selectedItem.price * $scope.quantity;
      //   $scope.clients[currentIndex].total += $scope.selectedItem.price * $scope.quantity;
      //   $scope.clients[currentIndex].products.push({
      //     served: false,
      //     productId: $scope.selectedItem._id,
      //     productName: $scope.selectedItem.name,
      //     price: $scope.selectedItem.price,
      //     quantity: $scope.quantity,
      //     subTotal: $scope.selectedItem.price * $scope.quantity
      //   });
      $scope.quantity = 0;
      $scope.selectedItem = {};
      // }
      $scope.clients[currentIndex].subTotal -= $scope.paying || 0;
      if ($scope.clients[currentIndex].subTotal < 0) $scope.clients[currentIndex].subTotal = 0;
      $scope.clients[currentIndex].comment = angular.copy($scope.comment);
      $scope.clients[currentIndex].toGo = angular.copy($scope.toGo);
      $scope.clients[currentIndex].paid = parseFloat($scope.clients[currentIndex].paid) +
                                          parseFloat($scope.paying || 0);
      if ($scope.clients[currentIndex].paid > $scope.clients[currentIndex].subTotal) {
        $scope.clients[currentIndex].paid = angular.copy($scope.clients[currentIndex].total);
      }
      $scope.paying = 0;
      $scope.comment = '';
      $scope.toGo = false;
      if ( $scope.clients[currentIndex].total === 0) {
        $scope.clients.splice(currentIndex, 1);
      }
      if (int === 0) {
        angular.element('#edit-selected-client-images').modal('close');
      } else {
        $scope.addClient();
      }
    };

    $scope.clearItemsArray = function () {
      $scope.comment = '';
      $scope.toGo = false;
      $scope.clients[currentIndex].subTotal = angular.copy(subTotal);
      $scope.clients[currentIndex].total = angular.copy(total);
      $scope.clients[currentIndex].products = angular.copy(products);
      if ($scope.selectedClient.total === 0) {
        $scope.clients.splice($scope.clients.length - 1, 1);
      }
    };

    $scope.resetClientsModal = function () {
      angular.element('#verify-close-modal').modal();
      angular.element('#verify-close-modal').modal('open');
    };

    $scope.resetClients = function () {
      angular.element('#new-sell-modal').modal('close');
      $scope.clients = [];
    };

    $scope.saveOrder = function () {
      preventSave = false;
      total = 0;
      pending = 0;
      if ($scope.clients.length === 0) {
        Materialize.toast('Favor de agregar información', 2000);
        return;
      }
      if ($scope.isGeneralSale && $scope.sellToGo) {
        Materialize.toast('Favor de seleccionar sólo una opción.', 1000);
        return;
      }

      if (!$scope.table && (!$scope.isGeneralSale && !$scope.sellToGo && !$scope.employeeFood)) {
        Materialize.toast('Favor de agregar número de mesa', 1000);
        return;
      }

      $scope.clients.forEach(function (client) {
        total += client.total;
        pending += client.subTotal;
        if (client.products.length === 0) {
          preventSave = true;
          return;
        }
      });

      if (preventSave) {
        Materialize.toast('Favor de agregar mínimo un producto por cliente', 1000);
        return;
      }
      if ($scope.sellToGo) {
        $scope.table = 9999;
      }

      if ($scope.isNew) {
        if ($scope.employeeFood && !$scope.selectedUser) {
          Materialize.toast('Seleccionar un usuario válido.', 1000);
          return;
        }

        if ($scope.employeeFood && $scope.selectedUser) {
          $scope.isGeneralSale = false;
          $scope.table = 9998;

        }
        if (preventMultipleSave) {
          Materialize.toast('La transacción ya está siendo procesada, favor de esperar', 2000);
          return;
        }
        preventMultipleSave = true;
        FoliosAPI.save({
          businessId: employmentId,
          userId: userId,
          userName: user,
          total: total,
          table: $scope.table || 0,
          pendingPayment: pending,
          details: $scope.clients,
          sendToKitchen: $scope.isGeneralSale,
          internalFood: $scope.employeeFood,
          internalUser: $scope.selectedUser,
          dayName: moment().format('dddd')
        }).$promise
        .then(function (res) {
          angular.element('#new-sell-modal').modal('close');
          preventMultipleSave = false;
          initialInfo();
        })
        .catch(function (err) {
          Materialize.toast('Ha ocurrido un error, intente más tarde', 1000);
          preventMultipleSave = false;
        });
      } else {
        if ($scope.employeeFood) {
          $scope.table = 9998;
        }
        if (preventMultipleSave) {
          Materialize.toast('La transacción ya está siendo procesada, favor de esperar', 2000);
          return;
        }
        preventMultipleSave = true;
        FoliosAPI.put({ id: sellId }, {
          businessId: employmentId,
          userId: userId,
          total: total,
          table: $scope.table || 0,
          pendingPayment: pending,
          details: $scope.clients,
          internalFood: $scope.employeeFood
        }).$promise
        .then(function (res) {
          angular.element('#new-sell-modal').modal('close');
          preventMultipleSave = false;
          if ($scope.employeeFood) {
            ticket = true;
          } else {
            ticket = false;
          }
          FoliosAPI.updateTicket({
            id: $scope.selectedFolio._id
          }, {
            ticket: ticket
          }).$promise
          .then(function (res) {
            $scope.selectedFolio.ticket = false;
            initialInfo();
          });
          $scope.clients = [];
        })
        .catch(function (err) {
          Materialize.toast('Ha ocurrido un error, intente más tarde', 1000);
          preventMultipleSave = false;
        });
      }
    };

    $scope.paySelectedFolio = function (item) {
      $scope.ticketMenu = 1;
      $scope.selectedFolioId = angular.copy(item._id);
      if (!item.products) {
        item.products = [];
      }
      $scope.selectedFolio = item;
      $scope.giveBack = 0;
      if (!item.ticket) {
        item.products = [];
        $scope.orderToShow = [];
        temporalProducts = [];
        $scope.checkIn = moment($scope.selectedFolio.date).format('hh:mm:a');
        $scope.elapsedTime = moment().diff($scope.selectedFolio.date, 'minutes');
        $scope.selectedFolio.details.forEach(function (detail) {
          detail.products.forEach(function (product) {
            var index = _.findIndex(item.products, {productId: product.productId});
            if (index >= 0) {
              item.products[index].quantity =
                parseFloat(item.products[index].quantity) +
                parseFloat(product.quantity);
            } else {
              item.products.push({
                portions: product.portions,
                productId: product.productId,
                productName: product.productName,
                price: product.price,
                quantity: parseFloat(product.quantity)
              });
            }
          });
        });
        angular.element('#print-ticket-modal').modal();
        angular.element('#print-ticket-modal').modal('open');
        return;
      }
      temppendingFolios = angular.copy($scope.pendingFolios);
      tempIndex = _.findIndex($scope.pendingFolios, { folioId: $scope.selectedFolio.folioId });
      angular.element('#pay-pending-modal').modal();
      angular.element('#pay-pending-modal').modal('open');
      angular.element('#client-paying').focus();
    };

    $('#print-ticket').click(function (e) {
      var content = document.getElementById('ticket-to-print').innerHTML;
      mywindow = window.open('', 'my div', 'width:70,height:70');
      mywindow.document.write('<html><head><title>Ticket</title>');
      mywindow.document.write('</head><body style="font-size: 12px !important">');
      mywindow.document.write(content);
      mywindow.document.write('</body></html>');
      mywindow.document.close(); // necessary for IE >= 10
      mywindow.focus(); // necessary for IE >= 10
      mywindow.print();
      mywindow.close();
    });

    $scope.printTicket = function () {
      if ($scope.selectedFolio.table === 9998) {
        $scope.employeeFood = true;
      } else {
        $scope.employeeFood = false;
      }

      $scope.selectedFolio.ticket = true;
      $scope.selectedFolioId = $scope.selectedFolio._id;
      ticketAPI.save({
        businessId: employmentId,
        userId: userId,
        userName: user,
        total: $scope.selectedFolio.total,
        table: $scope.selectedFolio.table,
        folio: JSON.stringify($scope.selectedFolio.folio),
        initialHour: $scope.checkIn,
        endHour: $scope.currentHour,
        totalTime: $scope.elapsedTime,
        products: $scope.selectedFolio.products,
        internalFood: $scope.employeeFood,
        modified: true
      }).$promise
      .then(function (res) {
        FoliosAPI.updateTicket({
          id: $scope.selectedFolioId
        }, {
          ticket: true
        }).$promise
        .then(function (res) {
          //imprimir ticket
          if ($scope.ticketMenu === 1) {
            temppendingFolios = angular.copy($scope.pendingFolios);
            tempIndex = _.findIndex($scope.pendingFolios, { folioId: $scope.selectedFolio.folioId });
            angular.element('#pay-pending-modal').modal();
            angular.element('#pay-pending-modal').modal('open');
            angular.element('#client-paying').focus();
          } else {
            $scope.selectedFolioId = angular.copy($scope.selectedFolio._id);
            angular.element('#order-details').modal();
            angular.element('#order-details').modal('open');
            $scope.orderToShow = [];
            temporalProducts = [];
            $scope.selectedFolio.details.forEach(function (detail) {

              orderSubTotal += parseFloat(detail.subTotal);
              detail.products.forEach(function(product) {

                temporalProducts.push({
                  productId: product.productId,
                  price: product.price,
                  productName: product.productName,
                  quantity: product.quantity,
                  subTotal: product.subTotal,
                  served: product.served,
                  printed: product.printed
                });
              });
              $scope.orderToShow.push({
                toGo: detail.toGo,
                paid: detail.paid,
                clientNumber: detail.clientNumber,
                products: temporalProducts,
                tempSubTotal: detail.subTotal,
                total: detail.total
              });
              temporalProducts = [];
            });
          }
        })
        .catch(function (err) {
          Materialize.toast('Ha ocurrido un error, intente más tarde', 1000);
        });
      });
      angular.element('#print-ticket-modal').modal('close');
    };

    $scope.cancelPayFolio = function () {
      $scope.pendingFolios = angular.copy(temppendingFolios);
      $scope.clientPaying = '';
    };

    $scope.confirmPayment = function () {
      var tempReturnPrice = 0;
      if (!$scope.selectedTableClient) {
        $scope.selectedFolio = $scope.pendingFolios[tempIndex];
        $scope.selectedFolio.pendingPayment -= parseFloat($scope.clientPaying);
        if ($scope.selectedFolio.pendingPayment < 0) {
          $scope.giveBack = $scope.selectedFolio.pendingPayment * - 1;
        $scope.selectedFolio.pendingPayment = 0;
      } else {
        $scope.giveBack = 0;
      }
      } else {
        var clientIndex =_.findIndex($scope.selectedFolio.details, $scope.selectedTableClient);
        $scope.selectedFolio.details[clientIndex].subTotal -=
        parseFloat($scope.clientPaying);

        if ($scope.selectedFolio.details[clientIndex].subTotal < 0) {
          tempReturnPrice = $scope.selectedFolio.details[clientIndex].subTotal * -1;
          $scope.giveBack = angular.copy(tempReturnPrice);
          $scope.selectedFolio.details[clientIndex].subTotal = 0;
        } else {
          $scope.giveBack = 0;
        }

        $scope.selectedFolio.pendingPayment -= parseFloat($scope.clientPaying - tempReturnPrice);
        if ($scope.selectedFolio.pendingPayment < 0) $scope.selectedFolio.pendingPayment = 0
        $scope.selectedFolio.details[clientIndex].paid += parseFloat($scope.clientPaying);
      }
      $scope.clientPaying = '';
    };

    $scope.savePayment = function () {
      
      if (parseFloat($scope.clientPaying) > 0) {
        $scope.confirmPayment();
      }
      if ($scope.selectedFolio.pendingPayment > 0) {
        FoliosAPI.updateBought({ id: $scope.selectedFolio._id }, {
          pendingPayment: $scope.selectedFolio.pendingPayment,
          details: $scope.selectedFolio.details,
          endDate: moment().format('DD-MM-YY HH:mm')
        }).$promise
        .then(function (res) {
          initialInfo();
        })
        .catch(function (err) {
          Materialize.toast('Ha ocurrido un error, intente más tarde', 1000);
        });
      } else {
        FoliosAPI.removeBought({ id: $scope.selectedFolio._id}).$promise
        .then(function (res) {
          initialInfo();
        })
        .catch(function (err) {
          Materialize.toast('Ha ocurrido un error, intente más tarde', 1000);
        });
      }
    };

    $scope.cancelCurrentOrderModal = function (folio) {
      selectedFolioToDelete = folio;
      angular.element('#cancel-order-modal').modal();
      angular.element('#cancel-order-modal').modal('open');
    };

    $scope.cancelCurrentOrder = function () {

      FoliosAPI.cancelBought({
         id: selectedFolioToDelete.folioId,
         cancelUserId: userId,
         cancelUser: user,
         table: selectedFolioToDelete.table
       }).$promise
      .then(function (res) {
        initialInfo();
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error, intente más tarde', 1000);
      });
    };

    $scope.openDetailPaymentModal = function (item, folioId, folio) {
      orderSubTotal = 0;
      $scope.selectedFolio = folio;
      $scope.ticketMenu = 2;
      if (!folio.products) {
        folio.products = [];
      }
      if (!folio.ticket && $scope.selectedRol !== 'n') {
        folio.products = [];
        $scope.orderToShow = [];
        temporalProducts = [];
        $scope.checkIn = moment($scope.selectedFolio.date).format('hh:mm:a');
        $scope.elapsedTime = moment().diff($scope.selectedFolio.date, 'minutes');
        $scope.selectedFolio.details.forEach(function (detail) {
          detail.products.forEach(function (product) {
            var index = _.findIndex(folio.products, {productId: product.productId});
            if (index >= 0) {
              folio.products[index].quantity =
                parseFloat(folio.products[index].quantity) +
                parseFloat(product.quantity);
            } else {
              folio.products.push({
                productId: product.productId,
                productName: product.productName,
                price: product.price,
                quantity: parseFloat(product.quantity)
              });
            }
          });
        });
        angular.element('#print-ticket-modal').modal();
        angular.element('#print-ticket-modal').modal('open');
        return;
      }
      angular.element('#order-details').modal();
      angular.element('#order-details').modal('open');
      $scope.orderToShow = [];
      temporalProducts = [];
      $scope.selectedFolioId = folioId;
      orderSubTotal = 0;
      item.forEach(function (detail) {

        orderSubTotal += parseFloat(detail.subTotal);
        detail.products.forEach(function(product) {

          temporalProducts.push({
            productId: product.productId,
            price: product.price,
            productName: product.productName,
            quantity: product.quantity,
            subTotal: product.subTotal,
            served: product.served,
            printed: product.printed
          });
        });
        $scope.orderToShow.push({
          toGo: detail.toGo,
          paid: detail.paid,
          clientNumber: detail.clientNumber,
          products: temporalProducts,
          tempSubTotal: detail.subTotal,
          total: detail.total
        });
        temporalProducts = [];
      });
    };

    $scope.aceptOrderDetailPayment = function () {
      orderSubTotal = 0;
      $scope.orderToShow.forEach(function (order) {
        orderSubTotal += order.subTotal;
      });

      if (orderSubTotal > 0) {
        FoliosAPI.updateBought({ id: $scope.selectedFolioId }, {
          pendingPayment: orderSubTotal,
          details: $scope.orderToShow
        }).$promise
        .then(function (res) {
          angular.element('#order-details').modal('close');
          initialInfo();
        })
        .catch(function (err) {
          Materialize.toast('Ha ocurrido un error, intente más tarde', 1000);
        });
      } else {
        FoliosAPI.removeBought({ id: $scope.selectedFolioId }).$promise
        .then(function (res) {
          angular.element('#order-details').modal('close');
          initialInfo();
        })
        .catch(function (err) {
          Materialize.toast('Ha ocurrido un error, intente más tarde', 1000);
        });
      }
    };

    $scope.setCategory = function (index) {
      $scope.selectedProductCategory = index;
    };

    initialInfo();
  }

  angular.module('ventas').controller('sellProductsCtrl', [
    '$scope',
    '$rootScope',
    '$state',
    'FoliosAPI',
    'ProductsAPI',
    'GlobalUserAPI',
    'FondoAPI',
    'ConfigAPI',
    'ProductCategoryAPI',
    'localStorageService',
    'SOCKET',
    'ticketAPI',
    'PrintersAPI',
    sellProductsCtrl
  ]);
}());
