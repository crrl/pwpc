(function () {
  'use strict';

   angular
  .module('ventas')
  .config(routeConfig);

  function routeConfig($stateProvider) {
    $stateProvider
    .state('sell-products', {
      url: '/ventas',
      parent:'root',
      access: ['a','m', 'n'],
      templateUrl: '/modules/ventas/client/views/sell-products.client.view.html',
      controller: 'sellProductsCtrl'
    })
    .state('serve-order', {
      url: '/pedidos',
      parent:'root',
      access: ['a','n', 'm'],
      templateUrl: '/modules/ventas/client/views/serve-order.client.view.html',
      controller: 'serveOrderCtrl'
    })
    .state('toserve-order', {
      url: '/entrega-pedidos',
      parent:'root',
      access: ['a','n', 'm'],
      templateUrl: '/modules/ventas/client/views/to-serve-order.client.view.html',
      controller: 'toServeOrderCtrl'
    })
    .state('order-history', {
      url: '/historial-pedidos',
      parent:'root',
      access: ['a'],
      templateUrl: '/modules/ventas/client/views/order-history.client.view.html',
      controller: 'orderHistoryCtrl'
    })
    .state('intern-history', {
      url: '/historial-interno',
      parent:'root',
      access: ['a'],
      templateUrl: '/modules/ventas/client/views/intern-history.client.view.html',
      controller: 'internHistoryCtrl'
    });
  }
}());
