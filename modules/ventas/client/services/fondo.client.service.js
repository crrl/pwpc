(function () {
  'use strict';

  function FondoAPI(API_URL, $resource) {
    return $resource(API_URL + '/fondo', {}, {
      put: {
        method: 'PUT'
      }
    });
  }

  angular.module('ventas').factory('FondoAPI', [
    'API_URL',
    '$resource',
    FondoAPI
  ]);
}());
