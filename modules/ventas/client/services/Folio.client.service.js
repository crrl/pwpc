(function () {
  'use strict';

  function FoliosAPI(API_URL, $resource) {
    return $resource(API_URL + '/folio/:id', {}, {
      put: {
        method: 'PUT'
      },
      updateBought: {
        url: API_URL + '/pendingFolio/:id',
        method: 'PUT'
      },
      updateTicket: {
        url: API_URL + '/folioPending/ticket/:id',
        method: 'PUT'
      },
      removeBought: {
        url: API_URL + '/pendingFolio/:id',
        method: 'DELETE'
      },
      getPendingService: {
        url: API_URL + '/servingPending',
        method: 'GET',
        isArray: true
      },
      updatePendingService: {
        url: API_URL + '/servingPending/:id',
        method: 'PUT'
      },
      getPartialPendingService: {
        url: API_URL + '/servingPendingDetail/:id',
        method: 'PUT'
      },
      cancelBought: {
        url: API_URL + '/cancelPendingFolio',
        method: 'PUT'
      },
      getHistory: {
        url: API_URL + '/folioHistory',
        method: 'GET',
        isArray: false
      },
      getInternHistory: {
        url: API_URL + '/folioInternHistory',
        method: 'GET',
        isArray: false
      },
      getFilteredHistory: {
        url: API_URL + '/folioHistory/:initialDate/:finalDate',
        method: 'GET',
        isArray: false
      },
      getFilteredInternHistory: {
        url: API_URL + '/folioInternHistory/:initialDate/:finalDate',
        method: 'GET',
        isArray: false
      },
      getSelectedHistory: {
        url: API_URL + '/folioHistory/:id',
        method: 'GET'
      },
      setTicketToTrue: {
        url: API_URL + '/folioTicket/:id',
        method: 'GET'
      },
      setTicketToTrueBack: {
        url: API_URL + '/folioTicketBack/:id',
        method: 'GET'
      }
    });
  }

  angular.module('ventas').factory('FoliosAPI', [
    'API_URL',
    '$resource',
    FoliosAPI
  ]);
}());
