(function () {
  'use strict';

  angular.module('insumo').config(routeConfig);

  function routeConfig($stateProvider) {
    $stateProvider
    .state('supplies-list', {
      url: '/listado-insumos',
      parent: 'root',
      access: ['a', 'm'],
      templateUrl: '/modules/supplies/client/views/supplies-list.client.view.html',
      controller: 'suppliesListCtrl'
    })
    .state('new-supply', {
      url: '/nuevo-insumo',
      parent: 'root',
      access: ['a', 'm'],
      templateUrl: '/modules/supplies/client/views/new-supply.client.view.html',
      controller: 'newSupplyCtrl'
    })
    .state('modify-supply', {
      url: '/modificar-insumo',
      parent: 'root',
      access: ['a', 'm'],
      params: {
        item: null,
        supplyType: null
      },
      templateUrl: '/modules/supplies/client/views/new-supply.client.view.html',
      controller: 'newSupplyCtrl'
    })
    .state('buy-supplies', {
      url: '/compra-insumos',
      parent: 'root',
      access: ['a','m'],
      templateUrl: '/modules/supplies/client/views/buy-supplies.client.view.html',
      controller: 'buySuppliesCtrl'
    })
    .state('supplies-details', {
      url:'/compras-detalles',
      parent: 'root',
      access: ['a', 'm'],
      templateUrl: '/modules/supplies/client/views/supplies-details.client.view.html',
      controller: 'suppliesDetailsCtrl'
    });
  }
}());
