(function () {
  'use strict';

  function buySuppliesCtrl($scope, $state, SuppliesDetailsAPI, SuppliesAPI, SecondarySuppliesAPI, GlobalUserAPI, localStorage) {
    $scope.currentItems = [];
    $scope.currentTotal = 0;
    var empresa = localStorage.get('empresa');
    var userId = localStorage.get('_id');
    function initialInfo() {
      GlobalUserAPI.query({ id: userId}).$promise
      .then(function (res) {
        $scope.usuario = res[0].user;
      });
      SecondarySuppliesAPI.query().$promise
      .then(function (res) {
        $scope.supplies = res;
        $scope.supplies.forEach(function (supply) {
          supply.nameWithDescription = supply.name + ' - ' + supply.supplyName;
        });
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error, intente más tarde.', 3000);
      });
    }


    $scope.addSuply = function () {
      if (!$scope.selectedSupply || !$scope.selectedSupply.originalObject) {
        Materialize.toast('Favor de seleccionar una opción antes de seguir.', 3000);
        return;
      }

      if (!$scope.quantity || !$scope.total) {
        Materialize.toast('Favor de llenar todos los campos.', 3000);
        return;
      }

      $scope.currentTotal += $scope.total;
      $scope.currentItems.push({
        id: $scope.selectedSupply.originalObject.supplyId,
        originalItemId: $scope.selectedSupply.originalObject._id,
        name: $scope.selectedSupply.originalObject.name,
        description: $scope.selectedSupply.originalObject.supplyName || '',
        quantity: $scope.quantity,
        portions: $scope.quantity * $scope.selectedSupply.originalObject.portions,
        total: $scope.total,
        price: ($scope.total/$scope.quantity)
      });

      $scope.total = '';
      $scope.quantity = '';
      $scope.$broadcast('angucomplete-alt:clearInput');
      angular.element('#supplies_value').focus();
    };

    $scope.deleteCurrentItem = function (index) {
      $scope.currentTotal -= $scope.currentItems[index].total;
      $scope.currentItems.splice(index,1);
    };

    $scope.checkEnterKey = function (ev) {
      if(ev.keyCode == 13){
        $scope.addSuply();
      }
    };

    $scope.returnToSuppliesDetailList = function () {
      $state.go('supplies-details');
    };

    $scope.saveTransaction = function () {
      if ($scope.currentTotal === 0) {
        Materialize.toast('Favor de agregar productos antes de finalizar.', 3000);
        return;
      }
      SuppliesDetailsAPI.save({
        businessId: empresa,
        userId: userId,
        user: $scope.usuario,
        products: $scope.currentItems,
        total: $scope.currentTotal
      }).$promise
      .then(function (res) {
        Materialize.toast('Se ha guardado correctamente.', 3000);
        $state.go('supplies-details');
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error, intente más tarde.', 3000);
      });
    };

    initialInfo();
  }

  angular.module('insumo').controller('buySuppliesCtrl', [
    '$scope',
    '$state',
    'SuppliesDetailsAPI',
    'SuppliesAPI',
    'SecondarySuppliesAPI',
    'GlobalUserAPI',
    'localStorageService',
    buySuppliesCtrl
  ]);
}());
