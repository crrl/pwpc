(function () {
  'use strict';

  function suppliesListCtrl($scope, $state, $filter, SuppliesAPI, GlobalUserAPI, localStorage) {
    var toDeleteItem;
    var userId = localStorage.get('_id');
    $scope.headers = ['Nombre', 'Porciones P/Producto', 'Categoria', 'Presentación', 'cantidad'];
    $scope.options = [{
      description: 'Kilogramo',
      type: 'K'
    }, {
      description: 'Litro',
      type: 'L'
    }, {
      description: 'Galon',
      type: 'G'
    }, {
      description: 'Carton',
      type: 'C'
    }, {
      description: 'Java',
      type: 'J'
    }, {
      description: 'Costal',
      type: 'S'
    }, {
      description: 'Bulto',
      type: 'B'
    }, {
      description: 'Unidad',
      type: 'U'
    }];

    function initialInfo() {
      GlobalUserAPI.query({ id: userId }).$promise
      .then(function (res) {
        $scope.selectedRol = res[0].rol;
        if ($scope.selectedRol === 'a') {
          $scope.actionIcons = [ {
            class: 'green-text',
            icon: 'edit',
            fn: $scope.modify
          }, {
            class: 'red-text',
            icon: 'delete',
            fn: $scope.openDeleteModal
          }];
        }
        SuppliesAPI.query().$promise
        .then(function (res) {
          $scope.supplies = res;
          $scope.suppliesToShow = [];
          $scope.supplies.forEach(function (supply) {
            supply.type = _.findWhere($scope.options, {type: supply.type});
            $scope.suppliesToShow.push({
              id: supply._id,
              name: supply.name,
              portions: supply.portionPerProduct || '0',
              category: supply.category,
              type: supply.type.description,
              quantity: supply.portions / supply.portionPerProduct 
            });
          });
        })
        .catch(function (err) {
          Materialize.toast('Ha ocurrido un error, favor de intentar más tarde.', 3000);
        });
      });
    }

    $scope.goToNewSupply = function () {
      $state.go('new-supply');
    };

    $scope.modify = function (item) {
      $state.go('modify-supply', {
        item: item,
        supplyType: _.findWhere($scope.options, {description: item.type})
      });
    };

    $scope.openDeleteModal = function (item) {
      toDeleteItem = item;
      angular.element('#delete-supply-modal').modal();
      angular.element('#delete-supply-modal').modal('open');
    };

    $scope.delete = function () {
      SuppliesAPI.remove({id: toDeleteItem.id}).$promise
      .then(function (res) {
        Materialize.toast('Se ha eliminado correctamente.', 3000);
        initialInfo();
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error, favor de intentar más tarde.', 3000);
      });
    };

    initialInfo();
  }

  angular.module('insumo').controller('suppliesListCtrl', [
    '$scope',
    '$state',
    '$filter',
    'SuppliesAPI',
    'GlobalUserAPI',
    'localStorageService',
    suppliesListCtrl
  ]);
}());
