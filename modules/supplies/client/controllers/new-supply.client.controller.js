(function () {
  'use strict';

  function newSupplyCtrl($scope, $state, SuppliesAPI, localStorage) {
    var selectedOption = {};
    $scope.options = [{
      description: 'Kilogramo(s)',
      type: 'K'
    }, {
      description: 'Litro(s)',
      type: 'L'
    }, {
      description: 'Galon(es)',
      type: 'G'
    }, {
      description: 'Carton(es)',
      type: 'C'
    }, {
      description: 'Java(s)',
      type: 'J'
    }, {
      description: 'Costal(es)',
      type: 'S'
    }, {
      description: 'Bulto(s)',
      type: 'B'
    }, {
      description: 'Unidad(es)',
      type: 'U'
    }];

    $scope.categories = [{
      description: 'Carnes',
    }, {
      description: 'Frutas y Verduras',
    }, {
      description: 'Variados',
    }];

    if ($state.params.item) {
      var item = $state.params.item;
      $scope.name = item.name;
      $scope.portions = item.portions || 0;
      $scope.selectedCategory = item.category || '';
      $scope.selectedType = $state.params.supplyType.type;
      selectedOption = _.findWhere($scope.options, { type: $scope.selectedType });
      $scope.title = 'Modificar insumo';
      $scope.saveSupply = function () {
        if (!$scope.name || !$scope.selectedType ||
          (!$scope.price && $scope.price < 1) ||
           !$scope.selectedCategory) {
          Materialize.toast('Favor de llenar todos los campos.', 3000);
          return;
        }
        selectedOption = _.findWhere($scope.options, { type: $scope.selectedType });
        SuppliesAPI.put({id: item.id},{
          name: $scope.name,
          portionPerProduct: $scope.portions,
          type: selectedOption.type,
          category: $scope.selectedCategory,
          typeDescription: selectedOption.description
        }).$promise
        .then(function (res) {
          Materialize.toast('Se ha guardado con éxito.', 3000);
          $state.go('supplies-list');
        })
        .catch(function (err) {
          Materialize.toast('Ha ocurrido un error, favor de intentar más tarde.', 3000);
        })
      };
    } else {
      var empresa = localStorage.get('empresa');
      $scope.title = 'Nuevo insumo';
      $scope.saveSupply = function () {
          selectedOption = _.findWhere($scope.options, { type: $scope.selectedType });
        if (!$scope.name || !$scope.selectedType ||
            (!$scope.price && $scope.price < 1)  ||
             !$scope.selectedCategory || $scope.portions < 1 ) {
          Materialize.toast('Favor de llenar todos los campos.', 3000);
          return;
        }
        SuppliesAPI.save({
          businessId: empresa,
          name: $scope.name,
          portionPerProduct: $scope.portions,
          type: selectedOption.type,
          category: $scope.selectedCategory,
          typeDescription: selectedOption.description
        }).$promise
        .then(function (res) {
          Materialize.toast('Se ha guardado con éxito.', 3000);
          $state.go('supplies-list');
        })
        .catch(function (err) {
          Materialize.toast('Ha ocurrido un error, favor de intentar más tarde.', 3000);
        })
      };
    }

    $scope.returnToSupplyList = function () {
      $state.go('supplies-list');
    };
  }

  angular.module('insumo').controller('newSupplyCtrl', [
    '$scope',
    '$state',
    'SuppliesAPI',
    'localStorageService',
    newSupplyCtrl
  ]);
}());
