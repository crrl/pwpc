(function () {
  'use strict';

  function suppliesDetailsCtrl($scope, $state, $filter, SuppliesDetailsAPI,
                               GlobalUserAPI, localStorage) {
    var userRol;
    var userId = localStorage.get('_id');
    var itemToDelete;

    function initialInfo() {
      GlobalUserAPI.get().$promise
      .then(function (res) {
        $scope.allUsers = res.users;
        userRol = _.findWhere($scope.allUsers, {_id: userId}).rol;
        if (userRol === 'a') {
          $scope.headers = ['Fecha', 'Responsable', 'Total'];
        } else {
          $scope.headers = ['Fecha', 'Responsable'];
        }
        SuppliesDetailsAPI.get({skip:0}).$promise
        .then(function (res) {
          $scope.result = res.result;
          $scope.count = res.count;
  
          $scope.items = [];

          $scope.allInfo = res.result;
          $scope.suppliesDetails = [];
          $scope.allInfo = $scope.allInfo.reverse();
          $scope.result.forEach(function (element) {
            var user = _.findWhere($scope.allUsers, {_id: element.userId});
            if (user) user = user.name;
            if (userRol === 'a') {
              $scope.suppliesDetails.push({
                id: element._id,
                date: moment(element.date).format('YYYY-MM-DD'),
                user: user || '',
                total: $filter('currency')(element.total || 0, '$', 2)
              });
            } else {
              $scope.suppliesDetails.push({
                date: moment(element.date).format('YYYY-MM-DD'),
                user: user || ''
              });
            }
          });

          if (userRol === 'a') {
            $scope.actionIcons = [{
              class: 'green-text',
              icon: 'remove_red_eye',
              tooltip: '',
              fn: function (elem) {
                $scope.selectedBoughtFolio = _.findWhere($scope.allInfo, { _id: elem.id });
                $scope.selectedUser = _.findWhere($scope.allUsers,
                  { _id: $scope.selectedBoughtFolio.userId }).name;
                angular.element('#selected-detail-modal').modal();
                angular.element('#selected-detail-modal').modal('open');
              }
            }, {
              class: 'red-text',
              icon: 'delete',
              tooltip: '',
              fn: function (item) {
                itemToDelete = item;
                angular.element('#remove-detail-modal').modal();
                angular.element('#remove-detail-modal').modal('open');
              }
            }];
          }
        })
        .catch( function () {
          Materialize.toast('Ha ocurrido un error, intentar más tarde.', 3000);
        });
      })
      .catch( function () {
        Materialize.toast('Ha ocurrido un error, intentar más tarde.', 3000);
      });

    }

    $scope.setPage = function(pageNumber, previousPage) {
      if(!pageNumber && !previousPage) {
        return;
      }
      SuppliesDetailsAPI.get({skip:(pageNumber*5) -5}).$promise
      .then(function (res) {
        $scope.result = res.result;
          $scope.count = res.count;
  
          $scope.items = [];

          $scope.allInfo = res.result;
          $scope.suppliesDetails = [];
          $scope.allInfo = $scope.allInfo.reverse();
          $scope.result.forEach(function (element) {
            var user = _.findWhere($scope.allUsers, {_id: element.userId});
            if (user) user = user.name;
            if (userRol === 'a') {
              $scope.suppliesDetails.push({
                id: element._id,
                date: moment(element.date).format('YYYY-MM-DD'),
                user: user || '',
                total: $filter('currency')(element.total || 0, '$', 2)
              });
            } else {
              $scope.suppliesDetails.push({
                date: moment(element.date).format('YYYY-MM-DD'),
                user: user || ''
              });
            }
          });

          if (userRol === 'a') {
            $scope.actionIcons = [{
              class: 'green-text',
              icon: 'remove_red_eye',
              tooltip: '',
              fn: function (elem) {
                $scope.selectedBoughtFolio = _.findWhere($scope.allInfo, { _id: elem.id });
                $scope.selectedUser = _.findWhere($scope.allUsers,
                  { _id: $scope.selectedBoughtFolio.userId }).name;
                angular.element('#selected-detail-modal').modal();
                angular.element('#selected-detail-modal').modal('open');
              }
            }, {
              class: 'red-text',
              icon: 'delete',
              tooltip: '',
              fn: function (item) {
                itemToDelete = item;
                angular.element('#remove-detail-modal').modal();
                angular.element('#remove-detail-modal').modal('open');
              }
            }];
          }
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error al cargar los datos.', 3000);
      });
    };

    $scope.buySupplies = function () {
      $state.go('buy-supplies');
    };

    $scope.delete = function () {
      SuppliesDetailsAPI.remove({id: itemToDelete.id}).$promise
      .then(function (res) {
        Materialize.toast('Se ha eliminado con éxito.', 3000);
        initialInfo();
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error, intente más tarde.', 3000);
        initialInfo();
      })
    };

    initialInfo();
  }

  angular.module('insumo').controller('suppliesDetailsCtrl', [
    '$scope',
    '$state',
    '$filter',
    'SuppliesDetailsAPI',
    'GlobalUserAPI',
    'localStorageService',
    suppliesDetailsCtrl
  ]);
}());
