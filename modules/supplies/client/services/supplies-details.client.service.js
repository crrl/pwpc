(function () {
  'use strict';
  function SuppliesDetailsAPI(API_URL, $resource) {
    return $resource(API_URL + '/supplies/details/:id',{},{
      put: {
        method: 'PUT'
      }
    });
  }

  angular.module('insumo').factory('SuppliesDetailsAPI', [
    'API_URL',
    '$resource',
    SuppliesDetailsAPI
  ]);
}());
