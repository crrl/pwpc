(function () {
  'use strict';
  function SuppliesAPI(API_URL, $resource) {
    return $resource(API_URL + '/supplies/:id',{},{
      put: {
        method: 'PUT'
      },
      getDetails: {
        url: API_URL + '/suppliesWithDetails',
        method: 'GET',
        isArray: true
      }
    });
  }

  angular.module('insumo').factory('SuppliesAPI', [
    'API_URL',
    '$resource',
    SuppliesAPI
  ]);
}());
