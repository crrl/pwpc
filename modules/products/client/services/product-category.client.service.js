(function () {
  'use strict';

  function ProductCategoryAPI(API_URL, $resoure) {
    return $resoure( API_URL + '/product-category/:id', {}, {
      put: {
        method: 'PUT'
      }
    });
  }

  angular.module('producto').factory('ProductCategoryAPI', [
    'API_URL',
    '$resource',
    ProductCategoryAPI
  ]);
}());
