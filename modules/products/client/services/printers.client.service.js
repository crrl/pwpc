(function () {
  'use strict';

  function PrintersAPI(API_URL, $resoure) {
    return $resoure( API_URL + '/printer/:id', {}, {
      put: {
        method: 'PUT'
      }
    });
  }

  angular.module('producto').factory('PrintersAPI', [
    'API_URL',
    '$resource',
    PrintersAPI
  ]);
}());
