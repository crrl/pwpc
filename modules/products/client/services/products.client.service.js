(function () {
  'use strict';

  function ProductsAPI(API_URL, PRINTER_URL, $resoure) {
    return $resoure( API_URL + '/products/:id', {}, {
      put: {
        method: 'PUT'
      },
      printKitcken: {
        method: 'POST',
        url: PRINTER_URL + '/print'
      }
    });
  }

  angular.module('producto').factory('ProductsAPI', [
    'API_URL',
    'PRINTER_URL',
    '$resource',
    ProductsAPI
  ]);
}());
