(function () {
  'use strict';
  angular.module('producto').config(routeConfig);

  function routeConfig($stateProvider) {
    $stateProvider
    .state('product-list', {
      url: '/listado-productos',
      parent: 'root',
      access: ['a'],
      templateUrl: '/modules/products/client/views/product-list.client.view.html',
      controller: 'productListCtrl'
    })
    .state('new-product', {
      url: '/nuevo-producto',
      parent: 'root',
      access: ['a'],
      templateUrl: '/modules/products/client/views/product.client.view.html',
      controller: 'productCtrl'
    })
    .state('modify-product', {
      url: '/modificar-producto',
      parent: 'root',
      access: ['a'],
      params: {
        item: null
      },
      templateUrl: '/modules/products/client/views/product.client.view.html',
      controller: 'productCtrl'
    })
    .state('category-list', {
      url: '/listado-categorias',
      parent: 'root',
      access: ['a'],
      templateUrl: '/modules/products/client/views/category-list.client.view.html',
      controller: 'categoryListCtrl'
    })
    .state('new-category', {
      url: '/nueva-categorias',
      parent: 'root',
      access: ['a'],
      templateUrl: '/modules/products/client/views/new-category.client.view.html',
      controller: 'newCategoryListCtrl'
    })
    .state('modify-category', {
      url: '/editar-categorias',
      parent: 'root',
      access: ['a'],
      params: {
        item: null
      },
      templateUrl: '/modules/products/client/views/modify-category.client.view.html',
      controller: 'modifyCategoryListCtrl'
    })
    .state('printers-list', {
      url: '/listado-impresoras',
      parent: 'root',
      access: ['a'],
      templateUrl: '/modules/products/client/views/printer-list.client.view.html',
      controller: 'printersListCtrl'
    })
    .state('new-printer', {
      url: '/nueva-impresora',
      parent: 'root',
      access: ['a'],
      templateUrl: '/modules/products/client/views/new-printer.client.view.html',
      controller: 'newPrinterCtrl'
    })
    .state('modify-printer', {
      url: '/editar-impresora',
      parent: 'root',
      access: ['a'],
      params: {
        item: null
      },
      templateUrl: '/modules/products/client/views/new-printer.client.view.html',
      controller: 'modifyPrinterCtrl'
    });
  }
}());
