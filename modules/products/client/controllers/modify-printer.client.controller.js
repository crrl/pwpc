(function () {
  'use strict';

  function modifyPrinterCtrl($scope, $state, PrintersAPI) {
    $scope.title = 'Modificar Impresora';

    function initialInfo() {
      $scope.item = $state.params.item;
      PrintersAPI.get({id: $state.params.item._id}).$promise
      .then(function (res) {
        $scope.printerName = res.name;
      });
    }

    $scope.returnToList = function () {
      $state.go('printers-list');
    };

    $scope.saveCategory = function () {
      if (!$scope.printerName) {
        Materialize.toast('Favor de llenar los campos requeridos.', 2000);
        return;
      }
      PrintersAPI.put({
        id: $scope.item._id
      }, {
        name: $scope.printerName
      }).$promise
      .then(function (res) {
        Materialize.toast('Se ha guardado con éxito.', 1000);
        $state.go('printers-list');
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error.', 1000);
      });
    };
    if ($state.params.item) {
      initialInfo();
    } else {
      $state.go('printers-list');
    }

  }

  angular.module('producto').controller('modifyPrinterCtrl', [
    '$scope',
    '$state',
    'PrintersAPI',
    modifyPrinterCtrl
  ]);
}());
