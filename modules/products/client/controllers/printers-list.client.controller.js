(function () {

  'use strict';

  function printersListCtrl($scope, $state, PrintersAPI) {
    $scope.headers = ['Impresoras'];
    var printersResponse;
    var printerToDelete;

    function initialInfo() {
      PrintersAPI.query().$promise
      .then(function (res) {
        $scope.printers = [];
        printersResponse = res;
        printersResponse.forEach(function (printer) {
          $scope.printers.push({
            _id: printer._id,
            nombre: printer.name
          });
        });
      })
      .catch(function (err) {
      });
    }

    $scope.addNewPrinter = function () {
      $state.go('new-printer');
    };

    $scope.modifyPrinter = function (item) {
      $state.go('modify-printer', { item: item});
    };

    $scope.openDeleteModal = function (item) {
      printerToDelete = item;
      angular.element('#delete-printer-modal').modal();
      angular.element('#delete-printer-modal').modal('open');
    };

    $scope.delete = function () {
      PrintersAPI.delete({ id: printerToDelete._id }).$promise
      .then(function () {
        Materialize.toast('Se ha eliminado con éxito.', 3000);
        initialInfo();
        printerToDelete = '';
      })
      .catch(function () {
        Materialize.toast('Ha ocurrido un error, intente más tarde.', 3000);
      });
    };

    initialInfo();
  }

  angular.module('producto').controller('printersListCtrl', [
    '$scope',
    '$state',
    'PrintersAPI',
    printersListCtrl
  ]);
}());
