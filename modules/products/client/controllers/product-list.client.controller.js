(function () {
  'use strict';

  function productListCtrl($scope, $state, $filter, ProductsAPI) {
    var itemToDelete;
    $scope.headers = ['Nombre', 'Precio'];

    function initialInfo() {
      ProductsAPI.query().$promise
      .then(function (res) {
        $scope.products = res;
        $scope.itemsToShow = [];
        $scope.products.forEach(function (element) {
          $scope.itemsToShow.push({
            id: element._id,
            name: element.name,
            price: $filter('currency')(element.price, '$', 2)
          });
        });
      })
      .catch(function (err) {
      });
    }

    $scope.addNewProduct = function () {
      $state.go('new-product');
    };

    $scope.modifyProduct = function (item) {
      $state.go('modify-product', { item: item});
    };

    $scope.openDeleteModal = function (item) {
      itemToDelete = item;
      angular.element('#delete-product-modal').modal();
      angular.element('#delete-product-modal').modal('open');
    };

    $scope.delete = function () {
      ProductsAPI.delete({id: itemToDelete.id}).$promise
      .then(function () {
        Materialize.toast('Se ha eliminado con éxito.', 3000);
        initialInfo();
        itemToDelete = '';
      })
      .catch(function () {
        Materialize.toast('Ha ocurrido un error, intente más tarde.', 3000);
      });
    };

    initialInfo();
  }

  angular.module('producto').controller('productListCtrl', [
    '$scope',
    '$state',
    '$filter',
    'ProductsAPI',
    productListCtrl
  ]);
}());
