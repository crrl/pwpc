(function () {
  'use strict';

  function productCtrl($scope, $state, ProductsAPI, SuppliesAPI, ProductCategoryAPI, localStorage) {
    $scope.selectedSupplies = [];
    function initialInfo() {
      SuppliesAPI.query().$promise
      .then(function (res) {
        $scope.supplies = res;
      });
      ProductCategoryAPI.query().$promise
      .then(function (res) {
        $scope.categories = res;
      });
    }

    if ($state.params.item) {
      var item = $state.params.item;
      ProductsAPI.get({id: item.id}).$promise
      .then(function (res) {
        $scope.categoryId = res.categoryId;
        $scope.selectedSupplies = res.portions;
        
      });
      $scope.title = "Modificar Producto";
      $scope.name = item.name;
      $scope.price = item.price.substring(1);
      $scope.categoryId = item.categoryId;
      $scope.saveTransaction = function () {
        if (!$scope.name || !$scope.categoryId || (!$scope.price || $scope.price < 1)
            || $scope.selectedSupplies.length <= 0) {
          Materialize.toast('Favor de llenar todos los campos.', 3000);
          return;
        }
        ProductsAPI.put({id: item.id},{
          name: $scope.name,
          price: $scope.price,
          categoryId: $scope.categoryId,
          portions: $scope.selectedSupplies
        }).$promise
        .then(function (res) {
          Materialize.toast('Se ha registrado éxitosamente.', 3000);
          $state.go('product-list');
        })
        .catch(function () {
          Materialize.toast('Ha ocurrido un error, intente más tarde.', 3000);
        });
      };
    } else {
      var empresa = localStorage.get('empresa');
      $scope.title = "Nuevo Producto";

      $scope.saveTransaction = function () {
        if (!$scope.name || !$scope.categoryId || (!$scope.price || $scope.price < 1)
            || $scope.selectedSupplies.length <= 0) {
          Materialize.toast('Favor de llenar todos los campos.', 3000);
          return;
        }
        ProductsAPI.save({
          businessId: empresa,
          name: $scope.name,
          price: $scope.price,
          categoryId: $scope.categoryId,
          portions: $scope.selectedSupplies
        }).$promise
        .then(function (res) {
          Materialize.toast('Se ha registrado éxitosamente.', 3000);
          $state.go('product-list');
        })
        .catch(function () {
          Materialize.toast('Ha ocurrido un error, intente más tarde.', 3000);
        });
      };
    }

    $scope.returnToList = function () {
      $state.go('product-list');
    };

    $scope.addSuply = function() {
      if (!$scope.supplyQuantity || !$scope.supplyToAdd) {
        Materialize.toast('Favor de llenar los campos requeridos.', 2000);
        return;
      }
      if (!_.find($scope.selectedSupplies, {id: $scope.supplyToAdd._id})) {
        $scope.selectedSupplies.push({
          id: $scope.supplyToAdd._id,
          name: $scope.supplyToAdd.name,
          quantity: $scope.supplyQuantity
        });
        $scope.supplyQuantity = 0;
        $scope.supplyToAdd = {};
      } else {
        Materialize.toast('El insumo seleccionado ya está en la lista.', 2000);
      }
    };

    $scope.delete = function(item) {
      var temporalIndex = _.findIndex($scope.selectedSupplies, { id: item.id });
      if (temporalIndex > -1) {
        $scope.selectedSupplies.splice(temporalIndex, 1);
      }
    };

    initialInfo();
  }

  angular.module('producto').controller('productCtrl', [
    '$scope',
    '$state',
    'ProductsAPI',
    'SuppliesAPI',
    'ProductCategoryAPI',
    'localStorageService',
    productCtrl
  ]);
}())
