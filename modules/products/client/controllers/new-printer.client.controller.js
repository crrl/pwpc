(function () {
  'use strict';

  function newPrinterCtrl($scope, $state, PrintersAPI) {
    $scope.title = 'Nueva Impresora';
    $scope.returnToList = function () {
      $state.go('printers-list');
    };

    $scope.saveCategory = function () {
      if (!$scope.printerName) {
        Materialize.toast('Favor de llenar los campos requeridos.', 2000);
        return;
      }
      PrintersAPI.save({
        printerName: $scope.printerName
      }).$promise
      .then(function (res) {
        Materialize.toast('Se ha guardado con éxito.', 1000);
        $state.go('printers-list');
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error.', 1000);
      });
    };

  }

  angular.module('producto').controller('newPrinterCtrl', [
    '$scope',
    '$state',
    'PrintersAPI',
    newPrinterCtrl
  ]);
}());
