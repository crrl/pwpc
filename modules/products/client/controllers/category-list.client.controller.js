(function () {

  'use strict';

  function categoryListCtrl($scope, $state, ProductCategoryAPI) {
    $scope.headers = ['Categoria'];
    var categoriesResponse;
    var categoryToDelete;

    function initialInfo() {
      ProductCategoryAPI.query().$promise
      .then(function (res) {
        $scope.categories = [];
        categoriesResponse = res;
        categoriesResponse.forEach(function (categorie) {
          $scope.categories.push({
            _id: categorie._id,
            nombre: categorie.category
          });
        });
      })
      .catch(function (err) {
      });
    }

    $scope.addNewCategory = function () {
      $state.go('new-category');
    };

    $scope.modifyCategory = function (item) {
      $state.go('modify-category', { item: item});
    };

    $scope.openDeleteModal = function (item) {
      categoryToDelete = item;
      angular.element('#delete-category-modal').modal();
      angular.element('#delete-category-modal').modal('open');
    };

    $scope.delete = function () {
      ProductCategoryAPI.delete({ id: categoryToDelete._id }).$promise
      .then(function () {
        Materialize.toast('Se ha eliminado con éxito.', 3000);
        initialInfo();
        categoryToDelete = '';
      })
      .catch(function () {
        Materialize.toast('Ha ocurrido un error, intente más tarde.', 3000);
      });
    };

    initialInfo();
  }

  angular.module('producto').controller('categoryListCtrl', [
    '$scope',
    '$state',
    'ProductCategoryAPI',
    categoryListCtrl
  ]);
}());
