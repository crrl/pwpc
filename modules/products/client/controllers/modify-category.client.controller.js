(function () {
  'use strict';

  function modifyCategoryListCtrl($scope, $state, ProductCategoryAPI, PrintersAPI) {
    $scope.item;
    var initialindex;

    $scope.images = [{
      name: 'Tacos',
      url: 'modules/core/client/img/comidas/Tacos.PNG'
    }, {
      name:'Torta',
      url: 'modules/core/client/img/comidas/torta.jpg'
    }, {
      name: 'Sandwich',
      url: 'modules/core/client/img/comidas/sandwich.jpg'
    }, {
      name: 'Sushi',
      url: 'modules/core/client/img/comidas/sushi.jpg'
    }, {
      name: 'Pizza',
      url: 'modules/core/client/img/comidas/pizza.jpg'
    }, {
      name: 'Antojitos',
      url: 'modules/core/client/img/comidas/antojitos.jpg'
    }, {
      name: 'Bebida',
      url: 'modules/core/client/img/comidas/bebida.jpg'
    }, {
      name: 'Arroz',
      url: 'modules/core/client/img/comidas/arroz.jpg'
    }, {
      name: 'Bagguet',
      url: 'modules/core/client/img/comidas/baguette.jpg'
    }, {
      name: 'Café',
      url: 'modules/core/client/img/comidas/cafe.jpg'
    }, {
      name: 'Carnes',
      url: 'modules/core/client/img/comidas/carne.jpg'
    }, {
      name: 'Crepa',
      url: 'modules/core/client/img/comidas/crepa.jpg'
    }, {
      name: 'Ensalada',
      url: 'modules/core/client/img/comidas/ensalada.png'
    }, {
      name: 'Frappe',
      url: 'modules/core/client/img/comidas/frappe.png'
    }, {
      name: 'Hamburguesa',
      url: 'modules/core/client/img/comidas/hamburguesa.jpg'
    }, {
      name: 'Hotdog',
      url: 'modules/core/client/img/comidas/hotdog.jpg'
    }, {
      name: 'Papas',
      url: 'modules/core/client/img/comidas/papas.jpg'
    }, {
      name: 'Pasta',
      url: 'modules/core/client/img/comidas/pasta.jpg'
    }, {
      name: 'Sopa',
      url: 'modules/core/client/img/comidas/sopa.png'
    }, {
      name: 'Otros',
      url: 'modules/core/client/img/comidas/otros.jpg'
    }];

    function initialInfo() {
      $scope.item = $state.params.item;

      PrintersAPI.query().$promise
      .then(function (res) {
        $scope.printers = res;
      });

      ProductCategoryAPI.get({id: $state.params.item._id}).$promise
      .then(function (res) {
        $scope.category = res.category;
        $scope.printerId = res.printer;
        initialindex = _.findIndex($scope.images, { url: res.image });
        $scope.selectedImage = angular.copy($scope.images[initialindex]);
      });
    }

    $scope.returnToList = function () {
      $state.go('category-list');
    };


    $scope.saveCategory = function () {
      if (!$scope.selectedImage || !$scope.category || !$scope.printerId) {
        Materialize.toast('Favor de llenar los campos requeridos.', 2000);
        return;
      }

      ProductCategoryAPI.put({
        id: $scope.item._id
      }, {
        category: $scope.category,
        image: $scope.selectedImage.url,
        printer: $scope.printerId
      }).$promise
      .then(function (res) {
        Materialize.toast('Se ha guardado con éxito.', 1000);
        $state.go('category-list');
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error en la transacción.', 1000);
      });
    };

    $scope.openImageModal = function () {
      angular.element('#image-select-modal').modal();
      angular.element('#image-select-modal').modal('open');
    };

    $scope.selectImage = function (image) {
      $scope.selectedImage = image;
      angular.element('#image-select-modal').modal('close');
    };

    if (!$state.params.item) {
      $state.go('category-list');
    } else {
      initialInfo();
    }
  }

  angular.module('producto').controller('modifyCategoryListCtrl', [
    '$scope',
    '$state',
    'ProductCategoryAPI',
    'PrintersAPI',
    modifyCategoryListCtrl
  ]);
}());
