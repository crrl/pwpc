(function (window) {
  'use strict';

  var applicationModuleName = 'PasswordPC';
  var API_URL = 'http://192.168.0.253:3002/api';
  var REPLICA_API_URL = 'http://192.168.0.253:3002/api';
  var SOCKET = io.connect('http://192.168.0.253:3002');
  var PRINTER_URL = 'http://localhost:4001/printer';

  var service = {
    applicationEnvironment: window.env,
    applicationModuleName: applicationModuleName,
    applicationModuleVendorDependencies: [
      'ngResource',
      'ngAnimate',
      'ngMessages',
      'ui.router',
      'ngFileUpload',
      'ui-notification',
      'ui.materialize',
      'LocalStorageModule',
      'angularUtils.directives.dirPagination',
      'angucomplete-alt',
      'AngularPrint',
      'ngIdle',
      'chart.js'

    ],
    registerModule: registerModule
  };

  window.ApplicationConfiguration = service;

  // Add a new vertical module
  function registerModule(moduleName, dependencies) {
    // Create angular module
    angular.module(moduleName, dependencies || [])
          .value('API_URL', API_URL)
          .value('REPLICA_API_URL', REPLICA_API_URL)
          .value('SOCKET', SOCKET)
	  .value('PRINTER_URL', PRINTER_URL);

    // Add the module to the AngularJS configuration file
    angular.module(applicationModuleName).requires.push(moduleName);
  }

  // Angular-ui-notification configuration
  angular.module('ui-notification').config(function(NotificationProvider) {
    NotificationProvider.setOptions({
      delay: 2000,
      startTop: 20,
      startRight: 10,
      verticalSpacing: 20,
      horizontalSpacing: 20,
      positionX: 'right',
      positionY: 'bottom'
    });
  });
}(window));
