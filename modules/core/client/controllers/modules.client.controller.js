(function () {
  'use strict';

    function modulesCtrl($scope, $state, ConfigAPI, GlobalUserAPI, localStorage) {
      $scope.items = [{
        description: 'Usuarios',
        icon: 'person',
        img: 'modules/core/client/img/home-icons/users.jpg',
        url: 'user-list'
      }, {
        description: 'Venta',
        icon: 'monetization_on',
        img: 'modules/core/client/img/home-icons/buy.png',
        url: 'sell-products'
      }, 
      {
        description: 'Pedidos',
        icon: 'monetization_on',
        img: 'modules/core/client/img/home-icons/pending.PNG',
        url: 'serve-order'
      }, {
        description: 'Entregas',
        icon: 'monetization_on',
        img: 'modules/core/client/img/home-icons/pending.PNG',
        url: 'toserve-order'
      }, {
        description: 'Nómina',
        icon: 'monetization_on',
        img: 'modules/core/client/img/home-icons/rosters.jpg',
        url: 'nomina-list'
      }, {
        description: 'Historial de ventas',
        icon: 'history',
        img: 'modules/core/client/img/home-icons/historial.PNG',
        url: 'order-history'
      }, {
        description: 'Historial de consumo interno',
        icon: 'history',
        img: 'modules/core/client/img/home-icons/historial.PNG',
        url: 'intern-history'
      }, {
        description: 'Listado de productos',
        icon: 'list',
        img: 'modules/core/client/img/home-icons/products.jpg',
        url: 'product-list'
      }, {
        description: 'Listado de insumos principales',
        icon: 'list',
        img: 'modules/core/client/img/home-icons/insumos.gif',
        url: 'supplies-list'
      }, {
        description: 'Listado de insumos secundarios',
        icon: 'list',
        img: 'modules/core/client/img/home-icons/insumos.gif',
        url: 'secondary-supplies-list'
      }, {
        description: 'Listado de Compras de Insumos',
        icon: 'list',
        img: 'modules/core/client/img/home-icons/compra-insumos.jpg',
        url: 'supplies-details'
      }, {
        description: 'Inventario',
        icon: 'toc',
        img: 'modules/core/client/img/home-icons/inventary.jpg',
        url: 'inventory-list'
      }, {
        description: 'Corte',
        icon: 'insert_chart',
        img: 'modules/core/client/img/home-icons/corte.PNG',
        url: 'sales-cut'
      }, {
        description: 'Caja grande',
        icon: 'monetization_on',
        img: 'modules/core/client/img/home-icons/big-box.jpg',
        url: 'payDesk'
      }, {
        description: 'Categorías',
        icon: 'settings',
        img: 'modules/core/client/img/home-icons/settings.png',
        url: 'category-list'
      }, {
        description: 'Impresoras',
        icon: 'settings',
        img: 'modules/core/client/img/home-icons/settings.png',
        url: 'printers-list'
      }, {
        description: 'Configuración',
        icon: 'settings',
        img: 'modules/core/client/img/home-icons/settings.png',
        url: 'config'
      }];
      var businessId = localStorage.get('empresa');
      var serial;

      GlobalUserAPI.getEndDate({ id: businessId }).$promise
      .then(function(res) {
        $scope.endDay = res.date;
      });

      $scope.goToState = function (url) {
        $state.go(url);
      };

      $scope.verifySerial = function () {
        serial = $scope.serial.split(' ').join('');
        GlobalUserAPI.updateEndDate({
          id: businessId,
          serial: serial
        }).$promise
        .then(function(res) {
          Materialize.toast('Se ha actualizado con éxito.', 2000);
          $scope.serial = '';
          serial = '';
        })
        .catch(function (err) {
          Materialize.toast('Ha ocurrido un error, intente más tarde.', 2000);
        });
      };

    }

  angular.module('core').controller('modulesCtrl', [
    '$scope',
    '$state',
    'ConfigAPI',
    'GlobalUserAPI',
    'localStorageService',
    modulesCtrl
  ]);
}());
