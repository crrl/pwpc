(function (){
  'use strict';

  function newBusinessCtrl($scope, $state, GlobalUserAPI) {
    $scope.saveTransaction = function () {
      if ($scope.user && $scope.user.length < 4) {
        Materialize.toast('El nombre de usuario debe tener al menos 4 caracteres.',3000);
        return;
      }
      if ($scope.password1 && $scope.password2 &&
          ($scope.password1.length < 7 || $scope.password1.length < 7)) {
        Materialize.toast('La contraseña debe ser de almenos 7 caracteres.',3000);
        return;
      }

      if (!$scope.businessName || !$scope.address ||
        !$scope.phone || !$scope.username ||
        !$scope.user || !$scope.password1 ||
        !$scope.password2) {
        Materialize.toast('Favor de introducir Los campos Marcados con "*".',3000);
        return;
      }
      if($scope.password1 !== $scope.password2) {
        Materialize.toast('Las contraseñas no coinciden.',3000);
        return;
      }
      var user = $scope.user.toLowerCase();
      GlobalUserAPI.setNewBusiness({
        businessName : $scope.businessName,
        businessEmail : $scope.email || '',
        businessAdress: $scope.address,
        businessPhone: $scope.phone,
        user: user,
        password: $scope.password1,
        rol: 'a',
        username: $scope.username
      }).$promise
      .then(function (res) {
        $state.go('login');
      })
      .catch(function (err){
      });
    };
  }

  angular.module('core').controller('newBusinessCtrl', [
    '$scope',
    '$state',
    'GlobalUserAPI',
    newBusinessCtrl
  ]);
}());
