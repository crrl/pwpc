(function () {
  'use strict';

  function loginCtrl ($scope, $rootScope, GlobalUserAPI, $state, localStorage) {
    var tempPass = '';
    var serial = '';
    var businessId = localStorage.get('empresa');
    
    if (!$rootScope.selectedBackgroundImage) {
      $rootScope.selectedBackgroundImage = '/modules/core/client/img/background.jpg';
    }

    function initialInfo() {
      GlobalUserAPI.getBusiness().$promise
      .then(function (res) {
        if (res[0]) {
          localStorage.set('empresa', res[0]._id);
          $rootScope.secNum = res[0].autogen;
        }
      });
    }

    $scope.checkEnterKey = function (ev) {
      if (ev.keyCode == 13) {
        $scope.loginVerify();
      }
    };
    $scope.loginVerify = function () {
      hashPassword();
      GlobalUserAPI.getBusiness().$promise
      .then(function (res) {
        localStorage.clearAll();
        localStorage.set('empresa', res[0]._id);
        localStorage.set('business', res[0]);
        $rootScope.secNum = res[0].autogen;
        var user = $scope.user && $scope.user.toLowerCase();
        GlobalUserAPI.post({
          user: user,
          password: tempPass
        }).$promise
        .then(function (res) {
          localStorage.set('token', res.token);
          localStorage.set('_id', res._id);
          localStorage.set('user', res.username);
          Materialize.toast('Bienvenid@ ' + res.username, 3000);
          $state.go('home');
        })
        .catch(function (err) {
          if (err.status === 500) {
            Materialize.toast('Favor de realizar pago antes de continuar.', 3000);
          } else {
            Materialize.toast('No se encontró usuario', 4000);
          }
          return;
        });
      });
    };

    $scope.openSerialModal = function (item, index) {
      angular.element('#new-serial-modal').modal();
      angular.element('#new-serial-modal').modal('open');
    };

    $scope.verifySerial = function () {
      serial = $scope.serial.split(' ').join('');
      GlobalUserAPI.updateEndDate({
        id: businessId,
        serial: serial
      }).$promise
      .then(function(res) {
        Materialize.toast('Se ha actualizado con éxito.', 2000);
        $scope.serial = '';
        serial = '';
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error, intente más tarde.', 2000);
      });
    };

    function hashPassword() {
      tempPass = window.btoa($scope.password);
    }

    if (!$rootScope.secNum) {
      initialInfo();
    }
  }

  angular.module('core').controller('loginCtrl', [
    '$scope',
    '$rootScope',
    'GlobalUserAPI',
    '$state',
    'localStorageService',
    loginCtrl
  ]);
}());
