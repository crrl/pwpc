(function() {
  'use strict';
  function HeaderController ($scope, $rootScope, $timeout, $state, $http, ConfigAPI, localStorage, Idle) {
    Idle.unwatch();

    var d = new Date();
    var weekday = [ "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"];
    var months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre",
                  "Octubre", "Noviembre", "Diciembre"];

    $scope.CurrentDay = weekday[d.getDay()];
    $scope.currentMonth = months[d.getMonth()];
    $scope.dayNumber = d.getDate();
    $scope.year = d.getFullYear();
    $timeout(function () {
          angular.element(document).ready(function(){
            Idle.watch();
          });
        });

    function reloadScreen() {
      if($rootScope.preventReload) {
        return;
      } else {
        $rootScope.preventReload = true;
        Idle.unwatch();
        $state.reload();
      }
    }


    $("body").click( function (e) {
      if (!(e.target).closest('#my-sidenav') &&
         e.target.className !== 'pointer togglemenu') {
        angular.element('#my-sidenav').removeClass('enabled');
        angular.element('#my-sidenav').addClass('disabled');
      }
    });

    if (!$rootScope.selectedBackgroundImage ||
          !$rootScope.selectedHeaderColor ||
          !$rootScope.selectedSellStyle ||
          !$rootScope.printOrders ||
          !$rootScope.saveOrders) {
        ConfigAPI.get({
          id: localStorage.get('_id')
        }).$promise
        .then(function (res) {
          $rootScope.selectedBackgroundImage = res.background ||
            '/modules/core/client/img/topbar.png';
          $rootScope.selectedHeaderColor = res.color ||
            '#183265';

          $rootScope.selectedSellStyle = res.sellUI || 0;
          $rootScope.printOrders = res.printOrders || false;

          $rootScope.saveOrders = res.saveOrders || false;
        })
        .catch(function (err) {
          Materialize.toast('Ha ocurrido un error cargando el estilo, intente más tarde.', 3000);
        });
      }

    $scope.$watch(function() {
      return $http.pendingRequests.length;
    }, function() {
      if ($http.pendingRequests.length > 0) {
        $rootScope.loadingPage = true;
      } else {
        $rootScope.loadingPage = false;
      }
    });

    // $scope.$on('Keepalive', function() {
    //   $http.get('/business',
    //     {timeout: 4000}
    //   ).then(function successCallback(response) {
    //     if ($rootScope.preventReload) $rootScope.preventReload = false;
    //     }, function errorCallback(response) {
    //       reloadScreen();
    //   });
    // });

    $scope.username = localStorage.get('user');

    $scope.closeSession = function () {
      localStorage.clearAll();
      $state.go('login');
    };

    $scope.toggleSidenav = function () {
      if (angular.element('#my-sidenav').hasClass('enabled')) {
        angular.element('#my-sidenav').removeClass('enabled');
        angular.element('#my-sidenav').addClass('disabled');
      } else {
        angular.element('#my-sidenav').addClass('enabled');
        angular.element('#my-sidenav').removeClass('disabled');
      }
    };
    function startTime() {
      var today = new Date();
      var h = today.getHours();
      var m = today.getMinutes();
      var s = today.getSeconds();
      m = checkTime(m);
      s = checkTime(s);
      document.getElementById('txt').innerHTML = h + ":" + m + ":" + s;
      var t = setTimeout(startTime, 500);
    }
    function checkTime(i) {
        if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
        return i;
    }

  startTime();

  }

  angular.module('core').controller('headerController', [
    '$scope',
    '$rootScope',
    '$timeout',
    '$state',
    '$http',
    'ConfigAPI',
    'localStorageService',
    'Idle',
    HeaderController
  ]).config(function(IdleProvider, KeepaliveProvider) {
    KeepaliveProvider.interval(8); // in seconds
  })
  .run(function(Idle){
    Idle.watch();
  });
}());
