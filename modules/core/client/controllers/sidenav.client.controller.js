(function () {
  'use strict';

  function sidenadCtrl($scope, $state) {
    $scope.items = [{
      description: 'Venta',
      icon: 'monetization_on',
      url: 'sell-products'
    }, {
      description: 'Estadísticas',
      icon: 'insert_chart',
      url: 'estadistics'
    }, {
      description: 'Faltante',
      icon: 'archive',
      url: 'shrinkage-list'
    }, {
      description: 'Usuarios',
      icon: 'person',
      url: 'user-list'
    }, {
      description: 'Pedidos',
      icon: 'monetization_on',
      url: 'serve-order'
    }, {
      description: 'Entregas',
      icon: 'monetization_on',
      url: 'toserve-order'
    }, {
      description: 'Nómina',
      icon: 'monetization_on',
      url: 'nomina-list'
    }, {
      description: 'Historial de ventas',
      icon: 'history',
      url: 'order-history'
    }, {
      description: 'Historial de consumo interno',
      icon: 'history',
      url: 'intern-history'
    }, {
      description: 'Listado de productos',
      icon: 'list',
      url: 'product-list'
    }, {
      description: 'Listado de insumos principales',
      icon: 'list',
      url: 'supplies-list'
    }, {
      description: 'Listado de insumos secundarios',
      icon: 'list',
      url: 'secondary-supplies-list'
    }, {
      description: 'Listado de Compras de Insumos',
      icon: 'list',
      url: 'supplies-details'
    }, {
      description: 'Inventario',
      icon: 'toc',
      url: 'inventory-list'
    }, {
      description: 'Corte',
      icon: 'insert_chart',
      url: 'sales-cut'
    }, {
      description: 'Caja grande',
      icon: 'monetization_on',
      url: 'payDesk'
    }, {
      description: 'Categorías',
      icon: 'settings',
      url: 'category-list'
    }, {
      description: 'Impresoras',
      icon: 'settings',
      url: 'printers-list'
    }, {
      description: 'Configuración',
      icon: 'settings',
      url: 'config'
    }];

    $scope.goToState = function (item, url) {
      angular.element('#my-sidenav').removeClass('enabled');
      angular.element('#my-sidenav').addClass('disabled');
      if (item) {
        $state.go(item.url);
      } else {
        $state.go(url);
      }
    };
  }

  angular.module('core').controller('sidenadCtrl', [
    '$scope',
    '$state',
    sidenadCtrl
  ]);

}());
