(function () {
  'use strict';

  function HomeController($scope, $state, localStorage, SOCKET) {

    var userId = localStorage.get('_id');
    var scrollPosition = 0;
    $scope.items = [];

    $(document).on('scroll', function() {
      // do your things like logging the Y-axis
      scrollPosition = angular.element(window.scrollY);
      if (scrollPosition[0] > 0) {
        angular.element('#topbar').removeClass('topbar-initial-position');
        angular.element('#topbar').addClass('topbar-fixed-position');
        angular.element('.principal-container').addClass('margin-top-44');
      } else {
        if (angular.element('#topbar').hasClass('topbar-fixed-position')) {
          angular.element('.principal-container').removeClass('margin-top-44');
          angular.element('#topbar').removeClass('topbar-fixed-position');
          angular.element('#topbar').addClass('topbar-initial-position');
        }
      }
    });
  }

  angular.module('core').controller('HomeController', [
    '$scope',
    '$state',
    'localStorageService',
    'SOCKET',
    HomeController
  ]);
}());
