(function () {
  'use strict';

  function routeConfig($stateProvider) {
    $stateProvider
    .state('admin', {
      abstract: true,
      url: '/admin',
      template: '<ui-view/>',
      data: {
        roles: ['admin']
      }
    });
  }

  angular
    .module('core.admin.routes')
    .config(routeConfig, ['$stateProvider']);
}());
