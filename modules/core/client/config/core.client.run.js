(function () {
  'use strict';

  angular.module('core').run(function($rootScope, $state, $timeout, $http,
                                      localStorageService, GlobalUserAPI) {
    moment.locale();
    $rootScope.$on('$stateChangeSuccess', function(event, toState) {

      $timeout(function () {
        angular.element(document).ready(function(){
          angular.element('.tooltipped').tooltip({delay: 50});
        });
      });
    });
    $rootScope.$on('$stateChangeStart', function(event, toState) {
      var empresa = localStorageService.get('empresa');
      var token = localStorageService.get('token');
      var moduleId = localStorageService.get('module');
      var id = localStorageService.get('_id');
      $rootScope.preventReload = true;
      $http.defaults.headers.common.Token = token;
      GlobalUserAPI.getEndDate({ id: empresa }).$promise
        .then(function (res) {
          var endDate = new Date(res.date);
          var currentDate = new Date();
          if (currentDate.getTime() > endDate.getTime()) {
            localStorageService.clearAll();
            return;
          }
        });
      if (!empresa) {
        if (toState.name !== 'new-empresa') event.preventDefault();
        GlobalUserAPI.getBusiness().$promise
        .then(function (res) {
          if (res[0]) {
            localStorageService.set('empresa', res[0]._id);
            $rootScope.secNum = res[0].autogen;
          }
          if (res.length === 0 && toState.name !== 'new-empresa') {
            $state.go('new-empresa');
          } else {
            $state.go('login');
          }
        });
      } else {
        GlobalUserAPI.query({id: id}).$promise
        .then(function(res) {
          var rol = res[0].rol;
          angular.element('.modal-overlay').remove();
          if (toState.name === 'new-empresa' && empresa) {
            $timeout(function() {
              $state.go('login');
            });
          } else if (toState.name !== 'login' && !token) {
            $timeout(function() {
              $state.go('login');
            });
          } else if (toState.name === 'login' && token) {
            $timeout(function() {
              $state.go('home');
            });
          } else if(toState.access && !toState.access.includes(rol)){
            Materialize.toast('Acceso denegado.', 3000);
            $state.go('home');
          } else if (token) {
            return;
          }
        })
        .catch(function(err) {
          if (err.status === 500 || err.status === 401) {
            localStorageService.clearAll();
          } else {
            Materialize.toast('Acceso denegado.', 3000);
          }
          if (!token) {
            $state.go('login');
          } else {
            $state.go('home');
          }
        });
      }
    });
  });
}());
