(function () {
  'use strict';

  function routeConfig($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
    $stateProvider
    .state('root', {
      templateUrl: '/modules/core/client/views/home.client.view.html',
      controller: 'HomeController',
      controllerAs: 'vm'
    })
    .state('home', {
      url: '/',
      parent: 'root',
      templateUrl: '/modules/core/client/views/modules.client.view.html',
      controller: 'modulesCtrl'
    })
    .state('forbidden', {
      url: '/forbidden',
      templateUrl: '/modules/core/client/views/403.client.view.html',
      data: {
        ignoreState: true,
        pageTitle: 'Forbidden'
      }
    })
    .state('login', {
      url: '/login',
      controller: 'loginCtrl',
      templateUrl: '/modules/core/client/views/login.client.view.html'
    })
    .state('new-empresa', {
      url: '/generar-datos-iniciales',
      // parent: 'root',
      templateUrl: '/modules/core/client/views/principal.client.view.html',
      controller: 'newBusinessCtrl'
    });
  }

  angular
    .module('core.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

}());
