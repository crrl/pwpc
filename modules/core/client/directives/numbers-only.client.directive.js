(function () {
  'use strict';

  function numbersOnly() {

    function link(scope, attrs, elem) {
      var trim = false;
      var index = -1;
      scope.string = scope.string || '';
      scope.$watch('string', function (ev) {
        if (ev <= -1) {
          scope.string = 0;
        }
        if (scope.string && scope.string.toString().includes('.')) {
            index = scope.string.toString().indexOf('.');
          if (scope.string.toString().split('.').length > 2) {
            trim = true;
          } else {
            trim = false;
          }
        } else {
          index = -1;
        }
        if (scope.string && scope.string.toString().substring(scope.string.length - 1) === '.' &&
            trim === false) {
          return;
        }
        if (scope.string) scope.string = parseFloat(scope.string);
        if (scope.string &&
            (isNaN(parseFloat(scope.string)) ||
             scope.string > scope.maxNumber) ||
            (index > 0 &&
            (scope.string.toString().length) > index + 3) ) {
          scope.string =
          parseFloat(
            (scope.string.toString()).substring(
              0,
              (scope.string.toString()).length - 1
            )
          );
        }
      });
    }

    return {
      restrict: 'A',
      scope: {
        string: '=',
        maxNumber: '='
      },
      link: link
    }
  }

  angular.module('core').directive('numbersOnly', [
    numbersOnly
  ]);
}());
