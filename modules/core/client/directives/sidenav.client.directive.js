(function (){
  'use strict';

  function sidenav() {
    return {
      restrict: 'E',
      templateUrl: 'modules/core/client/views/sidenav.client.view.html'
    }
  }

  angular.module('core').directive('mySidenav', sidenav);
}());
