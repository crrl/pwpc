(function () {
  'use strict';

  function Topbar() {

    function Link() {
      angular.element('.dropdown-button').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrain_width: false,
        hover: false,
        gutter: 0,
        belowOrigin: true
      });
    }

    return {
      restrict: 'E',
      templateUrl: 'modules/core/client/views/topbar.client.view.html',
      link: Link
    };
  }

  angular.module('core').directive('topbar', [
    Topbar
  ]);
}());
