(function () {
  'use strict';

  function catalogDirective() {

    return {
      restrict: 'AE',
      templateUrl: 'modules/core/client/views/catalog.client.view.html',
      scope: {
        items: '=',
        headers: '=',
        actionIcons: '=',
        title: '@',
        addFunction: '=',
        modify: '=',
        delete: '=',
        countItem: '=',
        pageControl: '&',
        filtro: '@'
      }
    }
  }

  angular.module('core').directive('catalog', [
    catalogDirective
  ]);
}());
