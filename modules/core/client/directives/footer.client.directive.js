(function () {
  'use strict';

  function footer() {

    return {
      restrict: 'E',
      templateUrl: 'modules/core/client/views/footer.client.view.html'
    };
  }

  angular.module('core').directive('myFooter', [
    footer
  ]);
}());
