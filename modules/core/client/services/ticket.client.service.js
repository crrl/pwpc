(function () {
  'use strict';

  function ticketAPI(API_URL, $resource) {
    return $resource(API_URL + '/ticket/:folio', {}, {
      put: {
        method: 'PUT'
      }
    });
  }

  angular.module('core').factory('ticketAPI', [
    'API_URL',
    '$resource',
    ticketAPI
  ]);
}());
