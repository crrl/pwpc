(function () {
  'use strict';
  function GlobalUserAPI($resource, API_URL) {

    return $resource(API_URL + '/globalUsers/:id', {}, {
      post: {
        url: API_URL + '/authenticate',
        method: 'POST'
      },
      getEndDate: {
        url: API_URL + '/authenticate/:id',
        method: 'GET'
      },
      updateEndDate: {
        url: API_URL + '/authenticate/:id',
        method: 'PUT'
      },
      getBusiness: {
        url: API_URL + '/business',
        method: 'GET',
        isArray: true
      },
      setNewBusiness: {
        url: API_URL + '/business',
        method: 'POST'
      },
      addNewUser: {
        url: API_URL + '/globalUsers',
        method: 'POST'
      },
      modifyUser: {
        url: API_URL + '/globalUsers/:id',
        method: 'PUT'
      }
    });
  }

  angular.module('core').factory('GlobalUserAPI', [
    '$resource',
    'API_URL',
    GlobalUserAPI
  ]);
}());
