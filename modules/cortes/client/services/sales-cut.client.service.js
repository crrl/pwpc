(function () {
  'use strict';

  function SalesCutAPI(API_URL, $resource) {
    return $resource(API_URL + '/salesCut/:id', {}, {
      put: {
        method: 'PUT'
      },
      generateXCut: {
        url: API_URL + '/generateXCut',
        method: 'POST'
      },
      generateZCut: {
        url: API_URL + '/generateZCut',
        method: 'POST'
      },
      generateTurnCut: {
        url: API_URL + '/generateTurnCut',
        method: 'POST'
      },
      getLastPaydesk: {
        url: API_URL + '/getLastPaydesk',
        method: 'GET'
      }
    });
  }

  angular.module('cortes').factory('SalesCutAPI', [
    'API_URL',
    '$resource',
    SalesCutAPI
  ]);
}());
