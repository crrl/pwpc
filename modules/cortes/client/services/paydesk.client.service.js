(function () {
  'use strict';

  function PaydeskAPI(API_URL, $resource) {
    return $resource(API_URL + '/paydesk/:id', {}, {
      put: {
        method: 'PUT'
      }
    });
  }

  angular.module('cortes').factory('PaydeskAPI', [
    'API_URL',
    '$resource',
    PaydeskAPI
  ]);
}());
