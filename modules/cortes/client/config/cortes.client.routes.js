(function () {
  'use strict';

  angular.module('cortes').config(routeConfig);

  function routeConfig($stateProvider) {
    $stateProvider
    .state('sales-cut', {
      url: '/corte',
      parent: 'root',
      access: ['a','m'],
      templateUrl: '/modules/cortes/client/views/sales-cut-list.client.view.html',
      controller: 'salesCutList'
    })
    .state('new-sales-cut', {
      url: '/nuevo-corte',
      parent: 'root',
      access: ['a','m'],
      templateUrl: '/modules/cortes/client/views/new-sale-cut.client.view.html',
      controller: 'newSaleCutCtrl'
    })
    .state('view-sales-cut', {
      url: '/detalle-corte',
      parent: 'root',
      access: ['a'],
      params: {
        sales: null
      },
      templateUrl: '/modules/cortes/client/views/view-sale-cut.client.view.html',
      controller: 'viewSaleCutCtrl'
    })
    .state('payDesk', {
      url: '/caja-grande',
      parent: 'root',
      access: ['a'],
      params: {
        sales: null
      },
      templateUrl: '/modules/cortes/client/views/pay-desk.client.view.html',
      controller: 'payDeskCtrl'
    });
  }
}());
