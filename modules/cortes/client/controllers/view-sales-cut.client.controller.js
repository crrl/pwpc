(function () {
  'use strict';

  function viewSaleCutCtrl($scope, $state, SalesCutAPI, SuppliesDetailsAPI, FoliosAPI, ticketAPI) {
    if (!$state.params.sales) {
      $state.go('sales-cut');
    }
    $scope.cutDetails = $state.params.sales;
    $scope.fondoTotal = 0;
    $scope.title = $scope.cutDetails.details.type;
    $scope.cutDetails.details.fondo.forEach(function (fondo) {
      $scope.fondoTotal += fondo.quantity;
    });

    $scope.goToList = function () {
      $state.go('sales-cut');
    };

    $scope.getSupplieFolio = function (id) {
      SuppliesDetailsAPI.get({'id': id}).$promise
      .then(function (res) {
        $scope.supplies = res;
        angular.element('#supply-detail-modal').modal();
        angular.element('#supply-detail-modal').modal('open');
      });
    };

    $scope.getProductFolio = function (id) {
      FoliosAPI.getSelectedHistory({'id': id}).$promise
      .then(function (res) {
        $scope.folios = res;
        angular.element('#folio-detail-modal').modal();
        angular.element('#folio-detail-modal').modal('open');
      });
    };

    $scope.getTickets = function (folio) {
      ticketAPI.query({folio:folio}).$promise
      .then( function (res) {
        $scope.tickets = res;
        angular.element('#ticket-list-modal').modal();
        angular.element('#ticket-list-modal').modal('open');
      });
    };
  }

  angular.module('cortes').controller('viewSaleCutCtrl', [
    '$scope',
    '$state',
    'SalesCutAPI',
    'SuppliesDetailsAPI',
    'FoliosAPI',
    'ticketAPI',
    viewSaleCutCtrl
  ]);
}());
