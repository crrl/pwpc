(function () {
  'use strict';

  function payDeskCtrl($scope, PaydeskAPI, localStorage) {
    function initialInfo() {
      PaydeskAPI.query().$promise
      .then(function (res) {
        if (res.length > 0) {
          $scope.paydesk = res[res.length - 1];
        }
        var response = res.reverse();
        $scope.discharge = [];
        $scope.charge = [];

        response.forEach(function (item, index) {
          if (item.compras > 0) {
            $scope.discharge.push({
              description: 'Compras',
              quantity: item.compras,
              date: item.date
            });
          }
          if (item.withdrawal > 0) {
            $scope.discharge.push({
              description: 'Retiros',
              quantity: item.withdrawal,
              date: item.date
            });
          }

          if (item.rosters > 0) {
            $scope.discharge.push({
              description: 'Nóminas',
              quantity: item.rosters,
              date: item.date
            });
          }

          if (item.deposit > 0) {
            $scope.charge.push({
              description: 'Depósitos',
              quantity: item.deposit,
              date: item.date
            });
          }

          if (item.sells > 0) {
            $scope.charge.push({
              description: 'Ventas',
              quantity: item.sells,
              date: item.date
            });
          }
        });
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error, intentar más tarde', 3000);
      });
    }

    $scope.retrieveMoney = function () {
      $scope.retrieve = 0;
      angular.element('#retrieve-money-modal').modal();
      angular.element('#retrieve-money-modal').modal('open');
    };

    $scope.retrieveMoneyApply = function () {
      if ($scope.retrieve < 0) {
        Materialize.toast('La cantidad no puede ser menor a 0.', 3000);
        return;
      }
      angular.element('#retrieve-money-modal').modal('close');
      var totalMoney = $scope.paydesk.fundAvailable - $scope.retrieve;
      PaydeskAPI.save({
        fundAvailable : totalMoney,
        previousFund: $scope.paydesk.fundAvailable,
        withdrawal: $scope.retrieve,
        deposit: 0,
        sells: 0,
        compras: 0,
        rosters: 0,
        cashFund: 0
      }).$promise
      .then(function (res) {
        Materialize.toast('Se ha guardado con éxito.', 3000);
        initialInfo();
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error, favor de intentar más tarde.', 3000);
        initialInfo();
      });
    }

    $scope.addMoney = function () {
      $scope.moneyToAdd = 0;
      angular.element('#add-money-modal').modal();
      angular.element('#add-money-modal').modal('open');
    };

    $scope.addMoneyApply = function () {
      if ($scope.moneyToAdd < 0) {
        Materialize.toast('La cantidad no puede ser menor a 0.', 3000);
        return;
      }
      angular.element('#add-money-modal').modal('close');
      var totalMoney = $scope.paydesk.fundAvailable + $scope.moneyToAdd;
      PaydeskAPI.save({
        fundAvailable : totalMoney,
        previousFund: $scope.paydesk.fundAvailable,
        withdrawal: 0,
        deposit: $scope.moneyToAdd,
        sells: 0,
        compras: 0,
        rosters: 0,
        cashFund: 0,
      }).$promise
      .then(function (res) {
        Materialize.toast('Se ha guardado con éxito.', 3000);
        initialInfo();
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error, favor de intentar más tarde.', 3000);
        initialInfo();
      });
    }

    initialInfo();
  }

  angular.module('cortes').controller('payDeskCtrl', [
    '$scope',
    'PaydeskAPI',
    'localStorageService',
    payDeskCtrl
  ]);
}());
