(function () {
  'use strict';

  function salesCutList($scope, $state, $filter, SalesCutAPI) {

    $scope.headers = ['Fecha', 'Responsable', 'Entrada', 'Salida', 'Utilidad', 'Tipo'];

    $scope.actionIcons = [{
      class: 'blue-text',
      icon: 'remove_red_eye',
      tooltip: '',
      fn: function (elem) {
        SalesCutAPI.query({id: elem.id}).$promise
        .then(function (res) {
          $scope.toShowSalesCut = {
            details: _.findWhere($scope.salesCutList, {_id:elem.id}),
            byUsers: res
          };
          $state.go('view-sales-cut', {sales: $scope.toShowSalesCut});
        });
      }
    }];

    function initialInfo() {
      SalesCutAPI.get({skip:0}).$promise
      .then(function (res) {
        $scope.salesCutList = res.result;
        $scope.totalItems = res.count;
        $scope.listToShow = [];

        $scope.salesCutList.forEach(function (saleCut) {
          $scope.listToShow.push({
            id: saleCut._id,
            date: moment(saleCut.date).format('DD-MM-YY HH:mm a'),
            name: saleCut.userName,
            entrie: $filter('currency') (saleCut.entries, '$', 2),
            outs: $filter('currency') (saleCut.Outputs + saleCut.totalRoster, '$', 2),
            utility: $filter('currency') (saleCut.entries - (saleCut.Outputs + saleCut.totalRoster), '$', 2),
            tipo: 'Corte ' + saleCut.type
          });
        });
        $scope.listToShow = $scope.listToShow.reverse();
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error, intente más tarde.', 3000);
      });
    }

    $scope.setPage = function(pageNumber, previousPage) {
      if(!pageNumber && !previousPage) {
        return;
      }
      SalesCutAPI.get({skip:(pageNumber*5) -5}).$promise
      .then(function (res) {
        $scope.salesCutList = res.result;
        $scope.totalItems = res.count;
        $scope.listToShow = [];

        $scope.salesCutList.forEach(function (saleCut) {
          $scope.listToShow.push({
            id: saleCut._id,
            date: moment(saleCut.date).format('DD-MM-YY HH:mm a'),
            name: saleCut.userName,
            entrie: $filter('currency') (saleCut.entries, '$', 2),
            outs: $filter('currency') (saleCut.Outputs + saleCut.totalRoster, '$', 2),
            utility: $filter('currency') (saleCut.entries - (saleCut.Outputs + saleCut.totalRoster), '$', 2),
            tipo: 'Corte ' + saleCut.type
          });
        });
        $scope.listToShow = $scope.listToShow.reverse();
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error, intente más tarde.', 3000);
      });
    };

    $scope.goToNewsaleCut = function () {
      $state.go('new-sales-cut');
    };

    initialInfo();
  }

  angular.module('cortes').controller('salesCutList', [
    '$scope',
    '$state',
    '$filter',
    'SalesCutAPI',
    salesCutList
  ]);
}());
