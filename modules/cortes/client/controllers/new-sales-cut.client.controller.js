
(function () {
  'use strict';
  function newSaleCutCtrl($scope, $state, $timeout, localStorage, SalesCutAPI, SuppliesDetailsAPI,
                          FoliosAPI, ticketAPI, GlobalUserAPI) {

    $scope.month = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
                    'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    $scope.monthShort = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
    $scope.weekdaysFull = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
    $scope.weekdaysLetter = ['D', 'L', 'M', 'X', 'J', 'V', 'S'];
    $scope.disable = [];
    $scope.today = 'Hoy';
    $scope.clear = 'Limpiar';
    $scope.close = 'Cerrar';
    var days = 15;
    var empresa = localStorage.get('empresa');
    var userId = localStorage.get('_id');
    var user = localStorage.get('user');
    GlobalUserAPI.get().$promise
      .then(function (res) {
        $scope.users = res.users;
        $scope.selectedRol = _.find(res.users, { _id: userId }).rol;
      });
    var mywindow;

    $scope.minDate = moment('2017-01-01').toISOString();
    $scope.maxDate = new Date().toISOString();

    $scope.goToList = function () {
      $state.go('sales-cut');
    };

    var generateZCut = function () {
      SalesCutAPI.generateZCut({
        businessId: empresa,
        userId: userId,
        username: user,
        type: 'Z'
      }).$promise
      .then(function(res) {
        $scope.cutDetails = res;
        $scope.title = 'Corte Z';
        localStorage.remove('fondo');
        Materialize.toast('Se ha generado corte Z', 3000);
        $scope.fondoTotal = 0;
        $scope.cutDetails.details.fondos.forEach(function (fondo) {
          $scope.fondoTotal += fondo.quantity;
        });
        $timeout(function () {
          var content = document.getElementById('cut-info').innerHTML;
          var content2 = document.getElementById('products').innerHTML;
          mywindow = window.open('', 'my div', 'width:70,height:70');
          mywindow.document.write('<html><head><title>Ticket</title>');
          mywindow.document.write('</head><body style="font-size: 12px !important">');
          mywindow.document.write('<div style="text-align: center;"> CORTE DE CAJA</div>');
          mywindow.document.write('<div style="text-align: center; font-weight: bolder">Corte a <br> ' + moment().format('DD-MM-YYYY hh:mm:a'));
          mywindow.document.write('<hr>');
          mywindow.document.write(content);
          mywindow.document.write('----------- <br/>');
          mywindow.document.write(content2);
          mywindow.document.write('</body></html>');
          mywindow.document.close(); // necessary for IE >= 10
          mywindow.focus(); // necessary for IE >= 10
          mywindow.print();
          mywindow.close();
        });
      })
      .catch(function (err) {
        Materialize.toast('No hay datos para generar corte requerido', 3000);
      });
    };

    $scope.generateTurnCut = function (error) {
      SalesCutAPI.generateTurnCut({
        businessId: empresa,
        userId: userId,
        username: user
      }).$promise
      .then(function(res) {
        $scope.cutDetails = res;
        $scope.title = 'Corte De Turno';
        localStorage.remove('fondo');
        Materialize.toast('Se ha generado corte de turno', 3000);
        $scope.fondoTotal = 0;
        $scope.cutDetails.details.fondos.forEach(function (fondo) {
          $scope.fondoTotal += fondo.quantity;
        });
        $timeout(function () {
          var content = document.getElementById('cut-info').innerHTML;
          var content2 = document.getElementById('products').innerHTML;
          mywindow = window.open('', 'my div', 'width:70,height:70');
          mywindow.document.write('<html><head><title>Corte de Turno</title>');
          mywindow.document.write('</head><body style="font-size: 12px !important">');
          mywindow.document.write('<div style="text-align: center;"> CORTE DE TURNO</div>');
          mywindow.document.write('<div style="text-align: center; font-weight: bolder">Corte a <br> ' + moment().format('DD-MM-YYYY hh:mm:a'));
          mywindow.document.write('<hr>');
          mywindow.document.write(content);
          mywindow.document.write('----------- <br/></div>');
          mywindow.document.write(content2);
          mywindow.document.write('</body></html>');
          mywindow.document.close(); // necessary for IE >= 10
          mywindow.focus(); // necessary for IE >= 10
          mywindow.print();
          mywindow.close();
        });
        if (!error) {
          generateZCut();
        }
      })
      .catch(function (err) {
        if (!error) {
          generateZCut();
        } else {
          Materialize.toast('No hay datos para generar corte requerido.', 3000);
        }
      });
    };

    $scope.generateSalesCut = function () {
      if ($scope.selectedCut === '2') {
        $scope.generateTurnCut(true);
      } else if ($scope.selectedCut === '1') {
        if (!$scope.initialDate || !$scope.finalDate) {
          Materialize.toast('Favor de seleccionar fechas válidas', 3000);
          return;
        } else if ($scope.selectedRol !== 'a') {
          Materialize.toast('Acceso denegado', 3000);
          return;
        }

        var initialDate = angular.copy($scope.initialDate);
        var finalDate = angular.copy($scope.finalDate);
        initialDate = initialDate.split('/');
        finalDate = finalDate.split('/');

        initialDate = initialDate[2] + '-' + initialDate[1] + '-' + initialDate[0];
        finalDate = finalDate[2] + '-' + finalDate[1] + '-' + finalDate[0];
        initialDate += ' '+ ($scope.initialHour || '00:00') + ':00.000Z';
        finalDate += ' '+ ($scope.finalHour || '23:59') + ':00.000Z';

        if (moment(initialDate).isAfter(moment(finalDate))) {
          Materialize.toast('Favor de seleccionar fechas válidas', 3000);
          return;
        }

        SalesCutAPI.generateXCut({
          initialDate: initialDate,
          finalDate: finalDate
        }).$promise
        .then(function(res) {
          $scope.cutDetails = [];
          $scope.cutDetails = res;
          $scope.title = 'Corte X';
          $scope.fondoTotal = 0;
          $scope.cutDetails.details.fondos.forEach(function (fondo) {
            $scope.fondoTotal += fondo.quantity;
          });
        })
        .catch(function (err) {
        });

      } else {
        $scope.generateTurnCut(false);
      }
    };

    $scope.getSupplieFolio = function (id) {
      SuppliesDetailsAPI.get({'id': id}).$promise
      .then(function (res) {
        $scope.supplies = res;
        angular.element('#supply-detail-modal').modal();
        angular.element('#supply-detail-modal').modal('open');
      });
    };

    $scope.getProductFolio = function (id) {
      FoliosAPI.getSelectedHistory({'id': id}).$promise
      .then(function (res) {
        $scope.folios = res;
        angular.element('#folio-detail-modal').modal();
        angular.element('#folio-detail-modal').modal('open');
      });
    };

    $scope.getTickets = function (folio) {
      ticketAPI.query({folio:folio}).$promise
      .then( function (res) {
        $scope.tickets = res;
        angular.element('#ticket-list-modal').modal();
        angular.element('#ticket-list-modal').modal('open');
      });
    };
  }

  angular.module('cortes').controller('newSaleCutCtrl', [
    '$scope',
    '$state',
    '$timeout',
    'localStorageService',
    'SalesCutAPI',
    'SuppliesDetailsAPI',
    'FoliosAPI',
    'ticketAPI',
    'GlobalUserAPI',
    newSaleCutCtrl
  ])
}());
