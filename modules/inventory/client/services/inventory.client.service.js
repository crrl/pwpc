(function () {
  'use strict';
  function InventoryAPI(API_URL, $resource) {
    return $resource(API_URL + '/inventory/:id', {}, {
      put: {
        method: 'PUT'
      }
    });
  }

  angular.module('inventario').factory('InventoryAPI', [
    'API_URL',
    '$resource',
    InventoryAPI
  ]);
}());
