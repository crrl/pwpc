(function () {
  'use strict';

  function inventoryListCtrl($scope, $state, InventoryAPI) {
    var lastId;
    $scope.headers = [ 'Fecha y hora', 'Responsable'];

    $scope.actionIcons = [{
      class: 'blue-text',
      icon: 'remove_red_eye',
      tooltip: '',
      fn: function (elem) {
        $scope.inventoryDetails = [];
        InventoryAPI.get({id: elem.id}).$promise
        .then(function (res) {
          $scope.inventoryDetails = res;
        });
        angular.element('#view-inventory-modal').modal();
        angular.element('#view-inventory-modal').modal('open');
      }
    }];

    function initialInfo() {
      InventoryAPI.get({skip:0}).$promise
      .then(function (res) {
        $scope.result = res.result;
        $scope.count = res.count;

        $scope.items = [];
        if (res.result.length === 0) {
          return;
        } 
        lastId = res.result[res.result.length - 1]._id;
        res.result = res.result.reverse();
        $scope.result.forEach(function (item) {
          $scope.items.push({
            id: item._id,
            date: moment(item.date).format('DD-MM-YYYY -- HH:mm'),
            user: item.userName
          });
        });
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error al cargar los datos.', 3000);
      });
    }

    $scope.setPage = function(pageNumber, previousPage) {
      if(!pageNumber && !previousPage) {
        return;
      }
      InventoryAPI.get({skip:(pageNumber*5) -5}).$promise
      .then(function (res) {
        $scope.result = res.result;
        $scope.count = res.count;

        $scope.items = [];
        if (res.result.length === 0) {
          return;
        } 
        lastId = res.result[res.result.length - 1]._id;
        res.result = res.result.reverse();
        $scope.result.forEach(function (item) {
          $scope.items.push({
            id: item._id,
            date: moment(item.date).format('DD-MM-YYYY -- HH:mm'),
            user: item.userName
          });
        });
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error al cargar los datos.', 3000);
      });
    };

    $scope.goToNewUser = function () {
      $state.go('new-inventory');
    };

    $scope.modify = function (item) {
      item.lastId = lastId;
      $state.go('edit-inventory', {item: item});
    };

    initialInfo();
  }

  angular.module('inventario').controller('inventoryListCtrl', [
    '$scope',
    '$state',
    'InventoryAPI',
    inventoryListCtrl
  ]);
}());
