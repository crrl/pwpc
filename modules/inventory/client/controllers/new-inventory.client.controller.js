(function () {
  'use strict';

  function newInventoryCtrl($scope, $state, InventoryAPI, SuppliesAPI, localStorage) {

    var empresa = localStorage.get('empresa');
    var user = localStorage.get('user');
    var userId = localStorage.get('_id');
    var currentItem;
    $scope.title = 'Nuevo Inventario';
    var item;
    $scope.isEnabled = true;
    function initialInfo() {
      if ($state.params.item) {
        $scope.title = 'Modificar Inventario';
        item = $state.params.item;
        InventoryAPI.get({id: item.id}).$promise
        .then(function (res) {
          $scope.itemsToEdit = res.details;
          SuppliesAPI.query().$promise
          .then(function (res) {
            $scope.items = res;
            $scope.lastId = $scope.items[$scope.items.length -1]._id;
            $scope.items.forEach(function (item, index) {
              $scope.items[index].newQuantity = _.findWhere($scope.itemsToEdit,
                                                     {insumoId: item._id}
                                                    );
              if ($scope.items[index].newQuantity) {
                $scope.items[index].newQuantity = $scope.items[index].newQuantity.quantityAfterSave || 0;
              } else {
                $scope.isEnabled = false;
              }
            });
          });
        })
        .catch(function (err) {
          Materialize.toast('Ha ocurrido un error, intente más tarde.', 3000);
        });
      } else {
        SuppliesAPI.query().$promise
        .then(function (res) {
          $scope.items = res;
        });
      }
    }

    $scope.SaveNewInventory = function () {
      var saveTransaction = true;
      var quantityConsumed = 0;
        if (item) {
          if (item.id !== item.lastId) {
            Materialize.toast('Sólo se puede actualizar el inventario más reciente.',3000);
            return;
          }
          var itemsToSend = $scope.itemsToEdit;
          itemsToSend.forEach(function (item) {
            currentItem = _.findWhere($scope.items, { _id: item.insumoId });
            
            item.name = currentItem.name +
            ' - ' + currentItem.typeDescription;
            item.quantityAfterSave = parseFloat(currentItem.newQuantity || 0);
            item.portionsAfterSave =  (currentItem.portionPerProduct * parseFloat(currentItem.newQuantity)) || 0;
            item.quantityConsumed = (item.quantityBeforeSave || 0 ) -
                                    parseFloat(currentItem.newQuantity || 0);
            
            if (!item.quantityConsumed || item.quantityConsumed < 0) {
              item.quantityConsumed = 0;
            }
          });
          InventoryAPI.put({'id': item.id}, {
            businessId: empresa,
            userId: userId,
            userName: user,
            details: itemsToSend
          }).$promise
          .then(function (res) {
            Materialize.toast('Se ha guardado con éxito.', 3000);
            $state.go('inventory-list');
          })
          .catch(function (err) {
            Materialize.toast('Ha ocurrido un error, intente más tarde.', 3000);
          });

        } else {
          var itemsToSend = [];
          $scope.items.forEach(function (item) {
            if (!item.newQuantity && item.newQuantity !== 0) {
              saveTransaction = false;
              return;
            } else {
              item.portionsAfterSave =  (item.portionPerProduct * parseFloat(item.newQuantity)) || 0;
              quantityConsumed = (item.quantity || 0 ) - parseFloat(item.newQuantity);
              if (quantityConsumed < 0) {
                quantityConsumed = 0;
              }
              itemsToSend.push({
                insumoId: item._id,
                quantityBeforeSave: item.quantity || 0,
                quantityAfterSave: parseFloat(item.newQuantity),
                quantityConsumed: quantityConsumed,
                name: item.name + ' - ' + item.typeDescription,
                portionsAfterSave: item.portionsAfterSave
              });
            }
          });

          if (!saveTransaction) {
            Materialize.toast('Favor de llenar todos los campos', 3000);
          } else {

          InventoryAPI.save({
            businessId: empresa,
            userId: userId,
            userName: user,
            details: itemsToSend
          }).$promise
          .then(function (res) {
            Materialize.toast('Se ha guardado con éxito.', 3000);
            $state.go('inventory-list');
          })
          .catch(function (err) {
            Materialize.toast('Ha ocurrido un error, intente más tarde.', 3000);
          });
        }
      }
    };

    $scope.returnToInventoryList = function () {
      $state.go('inventory-list');
    };

    initialInfo();
  }

  angular.module('inventario').controller('newInventoryCtrl', [
    '$scope',
    '$state',
    'InventoryAPI',
    'SuppliesAPI',
    'localStorageService',
    newInventoryCtrl
  ]);
}());
