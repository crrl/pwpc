'use strict';

angular.module('inventario').config(RouteConfig);

function RouteConfig($stateProvider) {
  $stateProvider
  .state('inventory-list', {
    url: '/listado-inventarios',
    parent: 'root',
    access: ['a', 'm'],
    controller: 'inventoryListCtrl',
    templateUrl: '/modules/inventory/client/views/inventory-list.client.view.html'
  })
  .state('new-inventory', {
    url: '/inventario',
    parent: 'root',
    access: ['a', 'm'],
    controller: 'newInventoryCtrl',
    templateUrl: '/modules/inventory/client/views/new-inventory.client.view.html'
  })
  .state('edit-inventory', {
    url: '/editar-inventario',
    parent: 'root',
    access: ['a', 'm'],
    params: {
      item: null
    },
    controller: 'newInventoryCtrl',
    templateUrl: '/modules/inventory/client/views/new-inventory.client.view.html'
  });
}
