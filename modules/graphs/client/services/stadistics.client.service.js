(function () {
  'use strict';

  function StadisticsAPI(API_URL, $resource) {
    return $resource(API_URL + '/stadistics', {}, {
      put: {
        method: 'PUT'
      },
      getStadistics: {
        method: 'POST'
      }
    });
  }

  angular.module('estadistics').factory('StadisticsAPI', [
    'API_URL',
    '$resource',
    StadisticsAPI
  ])
}());
