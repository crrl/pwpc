(function () {
  'use strict';

  angular.module('estadistics').config(routeConfig);

  function routeConfig($stateProvider) {
    $stateProvider
    .state('estadistics', {
      url: '/estadisticas',
      parent: 'root',
      access: ['a', 'administrador'],
      templateUrl: '/modules/graphs/client/views/stadistics.client.view.html',
      controller: 'stadisticsCtrl'
    });
  }
}());
