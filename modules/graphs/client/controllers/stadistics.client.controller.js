(function () {
  'use strict';

  function stadisticsCtrl($scope, $timeout, StadisticsAPI, localStorage) {
    var initialDate = '';
    var finalDate = '';
    var productArrayVerifier = 0;
    var sellPerDayVerifier = 0;
    var sellPerMonthVerifier = 0;
    var sellPerHourVerifier = 0;
    var commandsPerHoursVerifier = 0;
    var commandsPerMonthVerifier = 0;
    var diffHours = 0;
    var diffDays = 0;
    var diffMonths = 0;
    var diffFolioHours = 0;
    var diffFolioMonths = 0;
    var initialDateRange;
    var finalDateRange;
    var currentTime = new Date();
    $scope.currentTime = currentTime;
    $scope.month = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
                    'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    $scope.monthShort = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
    $scope.weekdaysFull = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
    $scope.weekdaysLetter = ['D', 'L', 'M', 'X', 'J', 'V', 'S'];
    $scope.disable = [];
    $scope.today = 'Hoy';
    $scope.clear = 'Limpiar';
    $scope.close = 'Cerrar';
    $scope.minDate = moment('2017-01-01').toISOString();
    $scope.maxDate = (new Date($scope.currentTime.getTime() + ( 1000 * 60 * 60 *24 * 1 ))).toISOString();


    $scope.daySellData = [];
    $scope.daySellLabels = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
    $scope.daySellSeries = [];

    $scope.totalMoneyAmmountData = [];
    $scope.totalMoneyAmmountLabels = angular.copy($scope.daySellLabels);
    $scope.totalMoneyAmmountSeries = [];

    $scope.totalMonthMoneyData = [];
    $scope.totalMonthMoneyLabels = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre',
                                    'Octubre', 'Noviembre', 'Diciembre'];
    $scope.totalMonthMoneySeries = [];

    $scope.hourMoneyData = [];
    $scope.hourMoneyLabels = ['0:00', '1:00', '2:00', '3:00', '4:00', '5:00', '1:00', '7:00', '8:00', '9:00',
                              '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00',
                              '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];
    $scope.hourMoneySeries = [];

    $scope.labels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012','2006', '2007', '2008', '2009', '2010', '2011', '2012'];
    var businessArray = [localStorage.get('business')._id];
    var fullBusiness = localStorage.get('business');

    $scope.colors = ['#72C02C', '#3498DB', '#717984', '#F1C40F'];

    function initialInfo() {
      initialDate = '';
      finalDate = '';

      $scope.daySellData = [];
      $scope.totalMoneyAmmountData = [];
      $scope.totalMonthMoneyData = [];
      $scope.hourMoneyData = [];
      $scope.commandsPerHours = [];
      $scope.commandsPerMonth = [];
      if (finalDateRange && initialDateRange) {
        finalDateRange = moment(finalDateRange).add(1,'day');
        initialDateRange = moment(initialDateRange);
      }
      StadisticsAPI.getStadistics({
        businessArray: businessArray,
        initialDate: initialDateRange,
        finalDate: finalDateRange,
        fullBusiness: fullBusiness
      }).$promise
      .then(function (res) {
        initialDateRange = '';
        finalDateRange = '';
        /*if (res.folios.length === 0) {
          Materialize.toast('No hay información para mostrar actualmente.', 3000);
          return;
        }*/
        $scope.response = res;
        
        //AQUI
        //var graphics = $scope.response.graphics;
        var graphics = $scope.response;
        $scope.daySellData = graphics.daySellData;
        $scope.totalMoneyAmmountData = graphics.totalMoneyAmmountData;
        $scope.totalMonthMoneyData = graphics.totalMonthMoneyData;
        $scope.hourMoneyData = graphics.hourMoneyData;
        $scope.commandsPerHours = graphics.commandsPerHours;
        $scope.commandsPerMonth = graphics.commandsPerMonth;
        $scope.daySellSeries = graphics.daySellSeries;
        //$scope.folios = graphics.folios;
        $scope.totalMoneyAmmountSeries = graphics.totalMoneyAmmountSeries;
        //HASTA AQUI
      });
    }

    $scope.filter = function () {
      if (!$scope.initialDate || !$scope.finalDate) {
        Materialize.toast('Favor de seleccionar fechas válidas', 3000);
        return;
      }

      initialDateRange = angular.copy($scope.initialDate);
      finalDateRange = angular.copy($scope.finalDate);

      initialDateRange = initialDateRange.split('/');
      finalDateRange = finalDateRange.split('/');

      initialDateRange = initialDateRange[2] + '-' + initialDateRange[1] + '-' + initialDateRange[0];
      finalDateRange = finalDateRange[2] + '-' + finalDateRange[1] + '-' + finalDateRange[0];

      if (moment(initialDateRange).diff(moment(finalDateRange), 'days') > 0) {
        Materialize.toast('Favor de seleccionar fechas válidas', 3000);
        return;
      }
      initialInfo();
    };
    initialInfo();
  }

  angular.module('estadistics').controller('stadisticsCtrl', [
    '$scope',
    '$timeout',
    'StadisticsAPI',
    'localStorageService',
    stadisticsCtrl
  ]);
}());
