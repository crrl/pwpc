(function () {
    'use strict';
  
    angular.module('insumo-secundario').config(routeConfig);
  
    function routeConfig($stateProvider) {
      $stateProvider
      .state('secondary-supplies-list', {
        url: '/listado-insumos-secundarios',
        parent: 'root',
        access: ['a', 'm'],
        templateUrl: '/modules/secondary-supplies/client/views/secondary-supplies-list.client.view.html',
        controller: 'secundarySuppliesListCtrl'
      })
      .state('new-secondary-supply', {
        url: '/nuevo-insumo-secundario',
        parent: 'root',
        access: ['a', 'm'],
        templateUrl: '/modules/secondary-supplies/client/views/new-secondary-supply.client.view.html',
        controller: 'newSecondarySupplyCtrl'
      })
      .state('modify-secondary-supply', {
        url: '/modificar-insumo-secundario',
        parent: 'root',
        access: ['a', 'm'],
        params: {
          item: null,
          supplyType: null
        },
        templateUrl: '/modules/secondary-supplies/client/views/new-secondary-supply.client.view.html',
        controller: 'newSecondarySupplyCtrl'
      });
    }
  }());
  