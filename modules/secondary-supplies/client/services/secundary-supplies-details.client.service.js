(function () {
    'use strict';
    function SecondarySuppliesAPI(API_URL, $resource) {
      return $resource(API_URL + '/secundary-supplies/:id',{},{
        put: {
          method: 'PUT'
        }
      });
    }
  
    angular.module('insumo').factory('SecondarySuppliesAPI', [
      'API_URL',
      '$resource',
      SecondarySuppliesAPI
    ]);
  }());
  