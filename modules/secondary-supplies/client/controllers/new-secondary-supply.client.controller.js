(function () {
    'use strict';
  
    function newSecondarySupplyCtrl($scope, $state, SecondarySuppliesAPI, SuppliesAPI, localStorage) {
      var selectedOption = {};
      $scope.selectedSupply = '';
      if ($state.params.item) {
        var item = $state.params.item;
        $scope.name = item.name;
        $scope.portions = item.portions || 0;
        $scope.supplyName = item.supplyName;

        $scope.title = 'Modificar insumo secundario';
        $scope.saveSupply = function () {
          if (!$scope.name || $scope.portions < 1 || !($scope.selectedSupply.originalObject && $scope.selectedSupply.originalObject._id)) {
            Materialize.toast('Favor de llenar todos los campos.', 3000);
            return;
          }
          SecondarySuppliesAPI.put({id: item.id},{
            name: $scope.name,
            portions: $scope.portions,
            supplyId: $scope.selectedSupply.originalObject._id,
            supplyName: $scope.selectedSupply.originalObject.name
          }).$promise
          .then(function (res) {
            Materialize.toast('Se ha guardado con éxito.', 3000);
            $state.go('secondary-supplies-list');
          })
          .catch(function (err) {
            Materialize.toast('Ha ocurrido un error, favor de intentar más tarde.', 3000);
          })
        };
      } else {
        var empresa = localStorage.get('empresa');
        $scope.title = 'Nuevo insumo secundario';
        $scope.saveSupply = function () {
          if (!$scope.selectedSupply || !$scope.selectedSupply.originalObject) {
            Materialize.toast('Favor de seleccionar un elemento de la lista de insumos', 2000);
            return;
          } else if (!$scope.name || isNaN($scope.portions) || $scope.portions < 1 || ($scope.selectedSupply.originalObject && !$scope.selectedSupply.originalObject._id)) {
            Materialize.toast('Favor de llenar todos los campos.', 3000);
            return;
          }
          SecondarySuppliesAPI.save({
            businessId: empresa,
            supplyId: $scope.selectedSupply.originalObject._id,
            name: $scope.name,
            portions: $scope.portions,
            supplyName: $scope.selectedSupply.originalObject.name
          }).$promise
          .then(function (res) {
            Materialize.toast('Se ha guardado con éxito.', 3000);
            $state.go('secondary-supplies-list');
          })
          .catch(function (err) {
            Materialize.toast('Ha ocurrido un error, favor de intentar más tarde.', 3000);
          })
        };
      }
      
      function initialFunction () {
        SuppliesAPI.query().$promise
        .then(function (res) {
          $scope.items = res;
        });
      }

      initialFunction();

      $scope.returnToSupplyList = function () {
        $state.go('secondary-supplies-list');
      };
    }
  
    angular.module('insumo-secundario').controller('newSecondarySupplyCtrl', [
      '$scope',
      '$state',
      'SecondarySuppliesAPI',
      'SuppliesAPI',
      'localStorageService',
      newSecondarySupplyCtrl
      
    ]);
  }());
  