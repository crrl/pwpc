(function () {
    'use strict';
  
    function secundarySuppliesListCtrl($scope, $state, $filter, SecondarySuppliesAPI, GlobalUserAPI, localStorage) {
      var toDeleteItem;
      var userId = localStorage.get('_id');
      $scope.headers = ['Nombre', 'Porciones P/Producto', 'Insumo principal'];
      function initialInfo() {
        
        GlobalUserAPI.query({ id: userId }).$promise
        .then(function (res) {
          $scope.selectedRol = res[0].rol;
          if ($scope.selectedRol === 'a') {
            $scope.actionIcons = [ {
              class: 'green-text',
              icon: 'edit',
              fn: $scope.modify
            }, {
              class: 'red-text',
              icon: 'delete',
              fn: $scope.openDeleteModal
            }];
          }
          SecondarySuppliesAPI.query().$promise
          .then(function (res) {
            $scope.supplies = res;
            $scope.suppliesToShow = [];
            $scope.supplies.forEach(function (supply) {
              supply.type = _.findWhere($scope.options, {type: supply.type});
              $scope.suppliesToShow.push({
                id: supply._id,
                idSupply: supply.supplyId,
                name: supply.name,
                portions: supply.portions || '0',
                supplyName: supply.supplyName
              });
            });
          })
          .catch(function (err) {
            Materialize.toast('Ha ocurrido un error, favor de intentar más tarde.', 3000);
          });
        });
      }
  
      $scope.goToNewSupply = function () {
        $state.go('new-secondary-supply');
      };
  
      $scope.modify = function (item) {
        
        $state.go('modify-secondary-supply', {
          item: item,
        });
      };
  
      $scope.openDeleteModal = function (item) {
        toDeleteItem = item;
        angular.element('#delete-supply-modal').modal();
        angular.element('#delete-supply-modal').modal('open');
      };
  
      $scope.delete = function () {
        
        SecondarySuppliesAPI.remove({id: toDeleteItem.id}).$promise
        .then(function (res) {
          Materialize.toast('Se ha eliminado correctamente.', 3000);
          initialInfo();
        })
        .catch(function (err) {
          Materialize.toast('Ha ocurrido un error, favor de intentar más tarde.', 3000);
        });
      };
  
      initialInfo();
    }
  
    angular.module('insumo-secundario').controller('secundarySuppliesListCtrl', [
      '$scope',
      '$state',
      '$filter',
      'SecondarySuppliesAPI',
      'GlobalUserAPI',
      'localStorageService',
      secundarySuppliesListCtrl
    ]);
  }());
  