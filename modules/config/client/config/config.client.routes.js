(function () {
  'use strict';
  function configRoutes($stateProvider) {
    $stateProvider
    .state('config', {
      url: '/configuracion',
      parent: 'root',
      templateUrl: 'modules/config/client/views/config.client.view.html',
      controller: 'configCtrl'
    })
  }

  angular.module('config').config(configRoutes);
}());
