(function () {
  'use strict';

  function configCtrl($scope, $rootScope, localStorage, ConfigAPI) {
    var businessId = localStorage.get('empresa');
    var userId = localStorage.get('_id');
    $scope.colors = [{
      name: 'Por defecto',
      color: '#183265'
    }, {
      name: 'Verde',
      color: '#157355'
    }, {
      name: 'Marrón',
      color: '#521717'
    }, {
      name: 'Naranja',
      color: '#dc6500'
    }, {
      name: 'Rojo',
      color: '#bf1515'
    }, {
      name: 'Purpura',
      color: '#7e15bf'
    }, {
      name: 'Azul',
      color: '#1588bf'
    }, {
      name: 'Negro',
      color: '#1f1f1f'
    }];

    $scope.images = [{
      name: 'Por defecto',
      img: '/modules/core/client/img/background.jpg'
    }, {
      name: 'BBS',
      img: '/modules/core/client/img/topbar.png'
    }, {
      name: 'Cómida méxicana',
      img: '/modules/core/client/img/background2.jpg'
    }, {
      name: 'Oscuro',
      img: '/modules/core/client/img/background3.jpg'
    }, {
      name: 'Blanco & negro',
      img: '/modules/core/client/img/background4.jpg'
    }, {
      name: 'Ilustración alimentos',
      img: '/modules/core/client/img/background5.jpg'
    }];

    $scope.sellStyle = [{
      name: 'Con imágenes',
      value: 1
    }, {
      name: 'Con texto',
      value: 0
    }];

    $scope.printTicket = [{
      name: 'Si',
      value: true
    }, {
      name: 'No',
      value: false
    }];

    $scope.showOrders = [{
      name: 'Si',
      value: true
    }, {
      name: 'No',
      value: false
    }];

    $scope.updateColor = function () {
      $rootScope.selectedHeaderColor = angular.copy($scope.selectedColor);
    };

    $scope.updateImage = function () {
      $rootScope.selectedBackgroundImage = angular.copy($scope.selectedImage);
    };

    $scope.updateStyle = function () {
      $rootScope.selectedSellStyle = angular.copy($scope.selectedStyle);
    };

    $scope.updatePrint = function () {
      $rootScope.printOrders = angular.copy($scope.selectedPrint);
    };

    $scope.updateOrders = function () {
      $rootScope.saveOrders = angular.copy($scope.selectedOrders);
    }

    $scope.updateConfig = function () {
      ConfigAPI.save({
        businessId: businessId,
        userId : userId,
        color: $rootScope.selectedHeaderColor || '#183265',
        background: $rootScope.selectedBackgroundImage || '/modules/core/client/img/topbar.png',
        sellUI: $rootScope.selectedSellStyle || 0,
        printOrders: $rootScope.printOrders || false,
        saveOrders: $rootScope.saveOrders || false
      }).$promise
      .then(function (res) {
        Materialize.toast('Se ha guardado correctamente.', 3000);
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error, intente más tarde.', 3000);
      });
    };
  }

  angular.module('config').controller('configCtrl', [
    '$scope',
    '$rootScope',
    'localStorageService',
    'ConfigAPI',
    configCtrl
  ]);
}());
