(function() {
  'use strict';

  function ConfigAPI(API_URL, $resource) {
    return $resource(API_URL + '/config/:id', {}, {
      put: {
        method: 'PUT'
      }
    });
  }

  angular.module('config').factory('ConfigAPI', [
    'API_URL',
    '$resource',
    ConfigAPI
  ]);
}());
