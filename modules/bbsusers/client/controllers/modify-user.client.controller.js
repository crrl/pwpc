(function () {
  'use strict';

  function modifyUserCtrl($scope, $state, GlobalUserAPI, localStorage) {
    $scope.title = 'Modificar Usuario';
    if ($state.params.id) {
      GlobalUserAPI.query({id:$state.params.id}).$promise
      .then(function (res) {
        $scope.user = res[0].user;
        $scope.username = res[0].name;
        $scope.selectedRol = res[0].rol;
        $scope.password1 = res[0].password;
        $scope.password2 = res[0].password;
      });
    } else {
      $state.go('user-list')
    }

    var businessId = localStorage.get('empresa');
    $scope.roles = [{
      description: 'Administrador',
      identifier: 'a'
    }, {
      description: 'Normal',
      identifier: 'n'
    }, {
      description: 'Moderador',
      identifier: 'm'
    }];

    $scope.returnToUserList = function () {
      $state.go('user-list');
    };

    $scope.SaveNewUser = function () {
      if (($scope.user && $scope.user.length < 4) || $scope.user.indexOf(' ') > -1) {
        Materialize.toast('El nombre de usuario debe tener al menos 4 caracteres y no debe tener espacios.', 3000);
        return;
      }
      if ($scope.password1 && $scope.password2 &&
          ($scope.password1.length < 7 || $scope.password1.length < 7)) {
        Materialize.toast('La contraseña debe ser de almenos 7 caracteres.', 3000);
        return;
      }

      if (!$scope.username || !$scope.selectedRol ||
          !$scope.user || !$scope.password1 ||
          !$scope.password2) {
        Materialize.toast('Favor de llenar todos los campos.',3000);
        return;
      }
      if ($scope.password1 !== $scope.password2) {
        Materialize.toast('Las contraseñas no coinciden.',3000);
        return;
      }
      var user = $scope.user.toLowerCase();
      GlobalUserAPI.modifyUser({id:$state.params.id},{
        user: user,
        password: $scope.password1,
        rol: $scope.selectedRol,
        name: $scope.username
      }).$promise
      .then(function (res) {
        Materialize.toast('Se ha modificado con éxito.',3000);
        $state.go('user-list');
      })
      .catch(function (err) {
        if (err.status === 401) {
          localStorage.clearAll();
        } else {
          Materialize.toast('Ha ocurrido un error.',3000);
        }
      })
    };
  }

  angular.module('usuarios').controller('modifyUserCtrl', [
    '$scope',
    '$state',
    'GlobalUserAPI',
    'localStorageService',
    modifyUserCtrl
  ])
}());
