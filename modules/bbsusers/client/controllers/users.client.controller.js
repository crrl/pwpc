(function () {
  'use strict';

  function usersCtrl($scope, $state, GlobalUserAPI, RosterAPI, localStorage) {
    $scope.headers = ['Nombre', 'Usuario', 'Rol', 'Fecha de Ingreso'];
    var selectedItem;
    var selectedPageNumber;
    $scope.actionIcons = [];

    var initialInfo = function () {
      GlobalUserAPI.get({skip:0}).$promise
      .then(function (res) {
        $scope.items = [];
        $scope.users = res.users;
        $scope.totalItems = res.count;
        $scope.users.forEach(function (element) {
          $scope.items.push({
            id: element._id,
            nombre: element.name,
            user: element.user,
            rol: element.rol,
            fechaIngreso: moment(element.joinDate).format('YYYY-MM-DD')
          });
        });
      });
    };

    $scope.setPage = function(pageNumber, previousPage) {
      selectedPageNumber = pageNumber;
      if(!pageNumber && !previousPage) {
        return;
      }
      GlobalUserAPI.get({skip:(pageNumber*5) -5}).$promise
      .then(function (res) {
        $scope.items = [];
        $scope.users = res.users;
        $scope.totalItems = res.count;
        $scope.users.forEach(function (element) {
          $scope.items.push({
            id: element._id,
            nombre: element.name,
            user: element.user,
            rol: element.rol,
            fechaIngreso: moment(element.joinDate).format('YYYY-MM-DD')
          });
        });
      });
    };

    $scope.goToNewUser = function () {
      $state.go('new-user');
    };

    $scope.modify = function (item, index) {
      if (index === 0 && selectedPageNumber === 1) {
        Materialize.toast('El usuario inicial no se puede modificar.', 3000);
        return;
      }
      $state.go('modify-user',{id:item.id});
    };

    $scope.openDeleteModal = function (item, index) {
      if (index === 0) {
        Materialize.toast('El usuario inicial no se puede eliminar.', 3000);
        return;
      }

      selectedItem = item.id;
      angular.element('#delete-user-modal').modal();
      angular.element('#delete-user-modal').modal('open');
    };

    $scope.delete = function () {
      GlobalUserAPI.delete({id: selectedItem}).$promise
      .then(function (res) {
        initialInfo();
        angular.element('#delete-user-modal').modal('close');
      });
    };
    initialInfo();
  }

  angular.module('usuarios').controller('usersCtrl', [
    '$scope',
    '$state',
    'GlobalUserAPI',
    'RosterAPI',
    'localStorageService',
    usersCtrl
  ]);
}());
