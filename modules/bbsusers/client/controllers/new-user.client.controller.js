(function () {
  'use strict';

  function newUserCtrl($scope, $state, GlobalUserAPI, localStorage) {
    $scope.title = 'Nuevo Usuario';
    var businessId = localStorage.get('empresa');
    $scope.roles = [{
      description: 'Administrador',
      identifier: 'a'
    }, {
      description: 'Normal',
      identifier: 'n'
    }, {
      description: 'Moderador',
      identifier: 'm'
    }];

    $scope.returnToUserList = function () {
      $state.go('user-list');
    };

    $scope.SaveNewUser = function () {
      if (($scope.user && $scope.user.length < 4) || $scope.user.indexOf(' ') > -1) {
        Materialize.toast('El nombre de usuario debe tener al menos 4 caracteres y no debe tener espacios.', 3000);
        return;
      }
      if ($scope.password1 && $scope.password2 &&
          ($scope.password1.length < 7 || $scope.password1.length < 7)) {
        Materialize.toast('La contraseña debe ser de almenos 7 caracteres.', 3000);
        return;
      }

      if (!$scope.username || !$scope.selectedRol ||
          !$scope.user || !$scope.password1 ||
          !$scope.password2) {
        Materialize.toast('Favor de llenar todos los campos.',3000);
        return;
      }
      if ($scope.password1 !== $scope.password2) {
        Materialize.toast('Las contraseñas no coinciden.',3000);
        return;
      }
      var user = $scope.user.toLowerCase();
      GlobalUserAPI.addNewUser({
        user: user,
        password: $scope.password1,
        rol: $scope.selectedRol,
        name: $scope.username,
        businessId: businessId
      }).$promise
      .then(function (res) {
        Materialize.toast('Se ha registrado con éxito.',3000);
        $state.go('user-list');
      })
      .catch(function (err) {
        if (err.status === 400) {
          Materialize.toast('Ese usuario ya existe.',3000);
        } else if (err.status === 401) {
          localStorage.clearAll();
        } else {
          Materialize.toast('Ha ocurrido un error.',3000);
        }
      })
    };
  }

  angular.module('usuarios').controller('newUserCtrl', [
    '$scope',
    '$state',
    'GlobalUserAPI',
    'localStorageService',
    newUserCtrl
  ])
}());
