(function () {
  'use strict';
  angular.module('usuarios').config(routeConfig);

  function routeConfig($stateProvider) {
    $stateProvider
    .state('user-list', {
      url: '/usuarios',
      parent: 'root',
      access: ['a'],
      templateUrl: '/modules/bbsusers/client/views/users.client.view.html',
      controller: 'usersCtrl'
    })
    .state('new-user', {
      url: '/nuevo-usuario',
      parent: 'root',
      access: ['a'],
      templateUrl: '/modules/bbsusers/client/views/new-user.client.view.html',
      controller: 'newUserCtrl'
    })
    .state('modify-user', {
      url: '/modificar-usuario',
      parent: 'root',
      access: ['a'],
      params: {
        id: null
      },
      templateUrl: '/modules/bbsusers/client/views/new-user.client.view.html',
      controller: 'modifyUserCtrl'
    });

  }

}());
