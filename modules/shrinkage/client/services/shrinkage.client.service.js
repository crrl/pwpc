(function () {
  'use strict';
  function shrinkageAPI(API_URL, $resource) {
    return $resource(API_URL + '/inventory/shrinkage/:date', {}, {
      put: {
        method: 'PUT'
      }
    });
  }
  angular.module('shrinkage').factory('shrinkageAPI', [
    'API_URL',
    '$resource',
    shrinkageAPI
  ]);
}());
