(function () {
  'use strict';
  angular.module('shrinkage').config(routeConfig);

  function routeConfig($stateProvider) {
    $stateProvider
      .state('shrinkage-list', {
        url: '/faltante',
        parent: 'root',
        access: ['a', 'administrador'],
        templateUrl: '/modules/shrinkage/client/views/shrinkage-list.client.view.html',
        controller: 'shrinkageListCtrl'
      });
  }
}());
