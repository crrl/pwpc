(function () {
  'use strict';

  function shrinkageListCtrl($scope, GlobalUserAPI, shrinkageAPI, Excel, $timeout, localStorage) {

    $scope.exportToExcel = function(tableId) { // ex: '#my-table'
      $scope.exportHref = Excel.tableToExcel(tableId, 'sheet name');
      $timeout(function() { location.href = $scope.exportHref; }, 100); // trigger download
    };

    var businessArray = [localStorage.get('business')];

    $scope.generate = function () {
      if (!$scope.selectedYear || !$scope.selectedMonth) {
        Materialize.toast('Favor de seleccionar una fecha válida', 2000);
      } else {
        shrinkageAPI.save({ date: $scope.selectedYear + '-' + $scope.selectedMonth,
                            businessArray: businessArray }).$promise
        .then(function (res) {
          $scope.itemsToGenerateReport = res.fullSupplies;
          $scope.dates = res.dates;
        })
        .catch(function (err) {
          Materialize.toast('No hay datos a mostrar.', 2500);
        });
      }
    };
  }

  angular.module('insumo').controller('shrinkageListCtrl', [
    '$scope',
    'GlobalUserAPI',
    'shrinkageAPI',
    'Excel',
    '$timeout',
    'localStorageService',
    shrinkageListCtrl
  ]);
}());
