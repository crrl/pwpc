(function () {
  'use strict';
  function nominaListCtrl($scope, $state, $filter, $timeout, RosterAPI,
                          localStorage) {

    var selectedItem;
    var empresa = localStorage.get('empresa');
    var userId = localStorage.get('_id');

    $scope.currentDay = moment().format('YYYY-MM-DD');
    $scope.headers = ['Inicio nómina', 'Usuario', 'Pago diario', 'Pago hora extra', 'Días trabajados', 'Horas extras', 'Total', 'Editar', 'Borrar', 'Pagar', 'faltas', 'Detalle'];
    $scope.ToSendRoster;

    $scope.actionIcons = [{
      class: 'green-text',
      icon: 'payment',
      tooltip: 'Pagar nómina',
      fn: function (elem) {
        $scope.ToSendRoster = angular.copy(elem);
        RosterAPI.get({rosterId: elem.id}).$promise
        .then(function (res) {
          $scope.ToSendRoster.actualAbsenceDays = res.absenceDays;
          $scope.ToSendRoster.initialDate = moment(res.initialDate).format('YYYY-MM-DD');
        })
        .catch(function (err) {
          Materialize.toast('Ha ocurrido un error, favor de intentar mas tarde.',3000);
        });
        angular.element('#pay-roster-modal').modal();
        angular.element('#pay-roster-modal').modal('open');
      }
    }, {
      class: 'blue-text',
      icon: 'date_range',
      fn: function(item) {
        $scope.removeAbsenceDays = '0';
        $scope.addAbsenceDays = '0';
        $scope.addExtraTime = '0';
        $scope.removeExtraTime = '0';

        $scope.ToSendRoster = angular.copy(item);
        angular.element('#modify-roster-modal').modal();
        angular.element('#modify-roster-modal').modal('open');
        RosterAPI.get({rosterId: item.id}).$promise
        .then(function (res) {
          $scope.ToSendRoster.actualAbsenceDays = res.absenceDays;
          $scope.ToSendRoster.initialDate = moment(res.initialDate).format('YYYY-MM-DD');
        })
        .catch(function (err) {
          Materialize.toast('Ha ocurrido un error, favor de intentar mas tarde.',3000);
        });
      }
    }, {
      class: 'blue-text',
      icon: 'search',
      fn: function (elem) {
        var user =_.findWhere($scope.usersWithId, {user: elem.user});
        RosterAPI.getUserRosters({id: user.id}).$promise
        .then(function (res) {
          $scope.rosters = res;
          $scope.rosters = $scope.rosters.reverse();
          angular.element('#show-user-rosters').modal();
          angular.element('#show-user-rosters').modal('open');
        })
        .catch(function (err) {
          if (err.status === 401) {
            localStorage.clearAll();
          } else {
            Materialize.toast('Ha ocurrido un error en la transacción.', 3000);
          }
        });
      }
    }];

    function initialInfo() {
    RosterAPI.query().$promise
    .then(function (res) {
      $scope.nominaList = res;
      $scope.nominaToShow = [];
      $scope.usersWithId = [];
      $scope.nominaList.forEach(function (nomina, index) {
        var daysWorked = moment($scope.currentDay).diff(moment(nomina.initialDate).format('YYYY-MM-DD'),'days');
        $scope.usersWithId.push({
          id: nomina.userId,
          user: nomina.username,
        });
        $scope.nominaToShow.push({
          initialDate: moment(nomina.initialDate).format('YYYY-MM-DD'),
          id: nomina._id,
          user: nomina.username,
          payPerDay: $filter('currency')(nomina.payPerDay || 0, '$',2),
          payPerExtraHour: $filter('currency')(nomina.payPerExtraHour || 0, '$',2),
          daysWorked: daysWorked - nomina.absenceDays,
          extraTime: nomina.extraTime || 0,
          total: $filter('currency') (
            (nomina.payPerDay * (daysWorked - nomina.absenceDays) || 0) +
            (nomina.payPerExtraHour * nomina.extraTime || 0), '$', 2
          )
        });
      });
    })
    .catch(function (err) {
      Materialize.toast('Ha ocurrido un error, intente de nuevo.',3000);
    });
    }

    $scope.goToNewRoster = function () {
      $state.go('new-roster');
    };

    $scope.modify = function (item) {
      $state.go('modify-roster', {item: item});
    }

    $scope.openDeleteModal = function (item) {
      selectedItem = item.id;
      angular.element('#delete-roster-modal').modal();
      angular.element('#delete-roster-modal').modal('open');
    };

    $scope.delete = function () {
      RosterAPI.delete({rosterId: selectedItem}).$promise
      .then(function (res) {
        Materialize.toast('Se ha eliminado con éxito.',3000);
        initialInfo();
        angular.element('#delete-roster-modal').modal('close');
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error.',3000);
      });
    };

    $scope.saveCurrentRoster = function () {
       if (parseFloat($scope.ToSendRoster.total.substring(1)) < 1) {
         Materialize.toast('Favor de verificar cantidad a pagar.', 3000);
         return;
       }
      RosterAPI.saveHistory({
        businessId: empresa,
        userId: userId,
        rosterId: $scope.ToSendRoster.id,
        username: $scope.ToSendRoster.user,
        workedDays: $scope.ToSendRoster.daysWorked,
        extraTime: $scope.ToSendRoster.extraTime,
        absenceDays: $scope.ToSendRoster.actualAbsenceDays,
        total: $scope.ToSendRoster.total.substring(1)
      }).$promise
      .then(function () {
        Materialize.toast('Transacción realizada con éxito.', 3000);
        initialInfo();
      })
      .catch(function () {
        Materialize.toast('Ha ocurrido un error, intente de nuevo más tarde', 3000);
        initialInfo();
      });
    };

    $scope.modifyCurrentRoster = function () {
      var totalAbsenseDays = $scope.ToSendRoster.actualAbsenceDays +
                        ($scope.addAbsenceDays || 0) -
                        ($scope.removeAbsenceDays || 0);
      var totalExtraTime = $scope.ToSendRoster.extraTime +
                      ($scope.addExtraTime || 0) -
                      ($scope.removeExtraTime || 0);
      var totalDaysWorked = $scope.ToSendRoster.daysWorked -
                       ($scope.addAbsenceDays || 0) +
                       ($scope.removeAbsenceDays || 0);

      if ( totalAbsenseDays < 0 ||
           totalExtraTime < 0 ||
           totalDaysWorked < 0)  {
        Materialize.toast('No puede haber números negativos.', 3000);
        return;
      }
      RosterAPI.put({rosterId: $scope.ToSendRoster.id}, {
      absenceDays: totalAbsenseDays,
      extraTime: totalExtraTime
    }).$promise
    .then(function (res) {
      Materialize.toast('Se ha realizado el cambio con éxito.', 3000);
      initialInfo();
      angular.element('#modify-roster-modal').modal('close');
    })
    .catch(function (err) {
      Materialize.toast('Ha ocurrido un error, intente más tarde.', 3000);
    });
    }

    initialInfo();
  }

  angular.module('nomina').controller('nominaListCtrl', [
    '$scope',
    '$state',
    '$filter',
    '$timeout',
    'RosterAPI',
    'localStorageService',
    nominaListCtrl
  ]);
}());
