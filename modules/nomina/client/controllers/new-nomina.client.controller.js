(function () {
  'use strict';

  function newNominaCtrl($scope, $state, RosterAPI, localStorage) {
    $scope.title = 'Nueva nómina';
    var empresa = localStorage.get('empresa');
    function initialInfo() {
      RosterAPI.getAvailableUsers().$promise
      .then(function (res) {
        $scope.userList = res;
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error en la transacción.',3000);
      });
    }

    $scope.createNewRoster = function () {
      if (!$scope.selectedUser || !$scope.payPerDay ||
          !$scope.extraHour) {
        Materialize.toast('Favor de llenar todos los datos.',3000);
        return;
      }
      RosterAPI.save({
        user: $scope.selectedUser._id,
        username: $scope.selectedUser.name,
        businessId: empresa,
        payPerDay: $scope.payPerDay,
        extraHour: $scope.extraHour
      }).$promise
      .then(function (res) {
        Materialize.toast('Se ha registrado la nómina con éxito.',3000);
        $state.go('nomina-list');
      })
      .catch(function (err) {
        Materialize.toast('El usuario ya tiene nómina asignada.',3000);
      });
    };

    $scope.returnToRosterList = function () {
      $state.go('nomina-list');
    }

    initialInfo();
  }

  angular.module('nomina').controller('newNominaCtrl', [
    '$scope',
    '$state',
    'RosterAPI',
    'localStorageService',
    newNominaCtrl
  ]);
}());
