(function () {

  'use strict';

  function modifyNominaCtrl($scope, $state, RosterAPI, localStorage) {
    var empresa;
    var item;
    function initialInfo() {
      $scope.title = 'Modificar nómina';
      empresa = localStorage.get('empresa');
      item = $state.params.item;
      item.name = item.user;
      $scope.userList = [{}];
      $scope.userList[0] = item;
      $scope.userList[0].name = item.user;
      $scope.selectedUser = item;
      $scope.selectedUser.name = item.user;
      $scope.payPerDay = item.payPerDay.substring(1);
      $scope.extraHour = item.payPerExtraHour.substring(1);
    }
    $scope.createNewRoster = function () {

      if (!$scope.selectedUser || !$scope.payPerDay ||
          !$scope.extraHour) {
        Materialize.toast('Favor de llenar todos los datos.',3000);
        return;
      }

      RosterAPI.put({rosterId: item.id},
        {
          payPerExtraHour: $scope.extraHour,
          payPerDay: $scope.payPerDay
      }).$promise
      .then(function (res) {
        Materialize.toast('Se ha actualizado la nómina con éxito.',3000);
        $state.go('nomina-list');
      })
      .catch(function (err) {
        Materialize.toast('El cambio no se pudo realizar.',3000);
      });
    };

    $scope.returnToRosterList = function () {
      $state.go('nomina-list');
    };

    if (!$state.params.item) {
      $state.go('nomina-list');
    } else {
      initialInfo();
    }
  }

  angular.module('nomina').controller('modifyNominaCtrl', [
    '$scope',
    '$state',
    'RosterAPI',
    'localStorageService',
    modifyNominaCtrl
  ]);
}());
