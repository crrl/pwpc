(function () {
  'use strict';
  function RosterAPI(API_URL, $resource) {
    return $resource(API_URL + '/roster/:rosterId', {}, {
      getAvailableUsers: {
        url: API_URL + '/rosters/available-users',
        method: 'GET',
        isArray: true
      },
      put: {
        method: 'PUT'
      },
      saveHistory: {
        url: API_URL + '/rosterHistory',
        method: 'POST'
      },
      getUserRosters: {
        url: API_URL + '/roster/globalUser/:id',
        method: 'GET',
        isArray: true
      }
    });
  }
  angular.module('nomina').factory('RosterAPI', [
    'API_URL',
    '$resource',
    RosterAPI
  ]);
}());
