(function () {
  'use strict';

  angular.module('nomina').config(routeConfig);

  function routeConfig($stateProvider) {
    $stateProvider
    .state('nomina-list', {
      url: '/listado-nomina',
      parent: 'root',
      access: ['a'],
      templateUrl: '/modules/nomina/client/views/nomina-list.client.view.html',
      controller: 'nominaListCtrl'
    })
    .state('new-roster', {
      url: '/nueva-nomina',
      parent: 'root',
      access: ['a'],
      templateUrl: '/modules/nomina/client/views/new-nomina.client.view.html',
      controller: 'newNominaCtrl'
    })
    .state('modify-roster', {
      url: '/modificar-nomina',
      parent: 'root',
      access: ['a'],
      params: {
        item: null
      },
      templateUrl: '/modules/nomina/client/views/edit-roster.client.view.html',
      controller: 'modifyNominaCtrl'
    });
  }
}());
