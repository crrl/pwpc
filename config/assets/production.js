'use strict';

/* eslint comma-dangle:[0, "only-multiline"] */

module.exports = {
  client: {
    lib: {
      css: [
        // bower:css
        'public/lib/materialize/dist/css/materialize.min.css',
        'public/lib/angular-ui-notification/dist/angular-ui-notification.css',
        'public/lib/angular-materialize/css/materialize.clockpicker.css',
        'public/lib/angucomplete-alt/angucomplete-alt.css',
        'public/lib/angularPrint/angularPrint.css',
        'public/lib/material-design-icons/iconfont/material-icons.css'
        // endbower
      ],
      js: [
        // bower:js
        'public/lib/jquery/dist/jquery.js',
        'public/lib/angular/angular.js',
        'public/lib/ng-idle/angular-idle.js',
        'public/lib/materialize/dist/js/materialize.min.js',
        'public/lib/angular-materialize/src/angular-materialize.js',
        'public/lib/angular-materialize/js/materialize.clockpicker.js',
        'public/lib/angular-animate/angular-animate.js',
        'public/lib/ng-file-upload/ng-file-upload.js',
        'public/lib/angular-messages/angular-messages.js',
        'public/lib/angular-mocks/angular-mocks.js',
        'public/lib/angular-resource/angular-resource.js',
        'public/lib/angular-ui-notification/dist/angular-ui-notification.js',
        'public/lib/angular-ui-router/release/angular-ui-router.js',
        'public/lib/owasp-password-strength-test/owasp-password-strength-test.js',
        'public/lib/angular-local-storage/dist/angular-local-storage.js',
        'public/lib/underscore/underscore-min.js',
        'public/lib/angularUtils-pagination/dirPagination.js',
        'public/lib/moment/moment.js',
        'public/lib/moment/locale/es.js',
        'public/lib/angucomplete-alt/angucomplete-alt.js',
        'public/lib/angularPrint/angularPrint.js'
        // endbower
      ]
    },
    css: 'public/dist/application*.min.css',
    js: 'public/dist/application*.min.js'
  }
};
